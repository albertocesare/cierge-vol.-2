//
//  detailTableViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 11/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import RealmSwift
import EventKit
import BRYXBanner

class detailTableViewController: UITableViewController {
    
    var arrayItems : [Booking] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControl?.addTarget(self, action: #selector(self.queryRealm), for: .valueChanged)
        
        
        let viewbg = UIView()
        viewbg.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.tableView.tableFooterView = viewbg

        self.queryRealm()
    }
    
    func queryRealm() {
        
        ParseBridge().syncBooking(completion: { (success) in
            
            if (success) {
                
                self.arrayItems.removeAll()
                
                let user = UserDefaults.standard
                let idHotel = user.string(forKey: "idHotel")
                
                let realm = try! Realm()
                let arr = realm.objects(Booking.self).sorted(byKeyPath: "date", ascending: true)
                print(arr.count)
                for object in arr {
                    if object.idHotel == idHotel! {
                        
                        
                        if object.date > Date() {
                            
                            self.arrayItems.append(object)
                            print(object)
                            
                        }
                        
                        
                    }
                }
                
                self.arrayItems = Array(Set(self.arrayItems))
                
                self.tableView.reloadData()
                
                self.refreshControl?.endRefreshing()
                
            }
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrayItems.count == 0 {
            let textView : UITextView = UITextView()
            textView.frame = CGRect(x: 20, y: self.view.frame.height / 2 - 50, width: self.view.frame.width - 40, height: 100)
            textView.textAlignment = .center
            textView.text = "In questa pagina visualizzerai le prenotazioni del soggiorno selezionato"
            textView.font = UIFont.systemFont(ofSize: 15)
            textView.textColor = .darkGray
            textView.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9803921569, alpha: 1)
            self.tableView.tableFooterView?.addSubview(textView)
        } else {
            let textView : UITextView = UITextView()
            textView.frame = CGRect(x: 20, y: self.view.frame.height / 2 - 50, width: self.view.frame.width - 40, height: 100)
            textView.textAlignment = .center
            textView.text = ""
            textView.font = UIFont.systemFont(ofSize: 15)
            textView.textColor = .darkGray
            textView.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9803921569, alpha: 1)
            self.tableView.tableFooterView?.addSubview(textView)
        }
        
        return arrayItems.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TTableViewCell
        let book = arrayItems[indexPath.row]
        
        cell.labelTitle.text = book.name
        
        let dateF = DateFormatter()
        dateF.dateFormat = "dd/MM/yyyy HH:MM"
        
        cell.labelSubtitle.text = dateF.string(from: book.date)
        
        let viewConfirm = UIView(frame: CGRect(x: 10, y: cell.frame.height / 2 - 10, width: 20, height: 20))
        viewConfirm.clipsToBounds = true
        viewConfirm.layer.cornerRadius = 10
        if book.state == "a" {
            viewConfirm.backgroundColor = #colorLiteral(red: 0, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        } else if book.state == "p" {
            viewConfirm.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        } else {
            viewConfirm.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
        cell.addSubview(viewConfirm)
        
        print(book.type)
        
        if book.type == "ins" {
        
        let query = PFQuery(className: "InBooking")
        query.whereKey("objectId", equalTo: book.id)
        query.findObjectsInBackground()
            { (objects, error) in
                
                if (objects?.first?["subServices"] as! String).components(separatedBy: "@").first == "3" {
                    
                    let viewGuest = UILabel()
                    viewGuest.frame = CGRect(x: cell.frame.width - 150, y: cell.frame.height - 30, width: 120, height: 20)
                    viewGuest.textAlignment = .right
                    viewGuest.text = "Ospiti: " + (objects?.first?["subServices"] as! String).components(separatedBy: "@").last!
                    viewGuest.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                    cell.addSubview(viewGuest)
                    
                }
        }
        
        } else if book.type == "rist" {
            
            let id = book.id
            let query = PFQuery(className: "RestBooking")
            query.whereKey("objectId", equalTo: id)
            query.findObjectsInBackground()
                {(objs, error) in
                    
                    let restId = objs?.first?["restaurantId"] as! String
                        
                       print(restId)
                    let realm = try! Realm()
                    let result = realm.objects(OutServices.self)
                    
                    for res in result {
                        if res.serviceId == restId {
                            cell.labelTitle.text = res.name
                        }
                    }
                    
            }
            
        } else {
            
            let id = book.id
            let query = PFQuery(className: "ActivBooking")
            query.whereKey("objectId", equalTo: id)
            query.findObjectsInBackground()
                {(objs, error) in
                    
                    let restId = objs?.first?["activityId"] as! String
                    
                    print(restId)
                    let realm = try! Realm()
                    let result = realm.objects(OutServices.self)
                    
                    for res in result {
                        if res.serviceId == restId {
                            cell.labelTitle.text = res.name
                        }
                    }
                    
            }
            
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    @IBAction func dismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let id = arrayItems[indexPath.row].id
        
        let query = PFQuery(className: "InBooking")
        query.whereKey("objectId", equalTo: id)
        query.findObjectsInBackground()
            { (objects, error) in
                
                if let info = (objects?.first?["subServices"] as? String) {
                if (info.components(separatedBy: "@").first == "1") || (info.components(separatedBy: "@").first == "2") {
                    
                    var stringMsn = ""
                    
                    for obj in (info.components(separatedBy: "@").last?.components(separatedBy: "*"))! {
                        
                        stringMsn = stringMsn + "\n" + obj.components(separatedBy: "_").first!
                        
                    }
                    
                    let alertController = UIAlertController(title: "Articoli prenotati", message: stringMsn, preferredStyle: UIAlertControllerStyle.alert)
                    let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
                        (action : UIAlertAction!) -> Void in
                        
                    })
                    
                    alertController.addAction(cancelAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
        }
        
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView,
                            editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let obj = arrayItems[indexPath.row]
        if obj.state == "d" {
        
        let more = UITableViewRowAction(style: .normal, title: "Cancella") { action, index in
            
            let pfobj = self.arrayItems[indexPath.row]
            var query = PFQuery(className:"InBooking")
            
            if (pfobj.type == "rist") {
                
                query = PFQuery(className:"RestBooking")
                
            } else  if (pfobj.type == "ins") {
                
                query = PFQuery(className:"InBooking")
                
                
            } else  if (pfobj.type == "act") {
                
                query = PFQuery(className:"ActivBooking")
                
                
            }
            
            //self.arrayItems.remove(at: indexPath.row)
            
            
            query.getObjectInBackground(withId: pfobj.id, block: { (object, error) -> Void in
                
                object?.deleteInBackground()
                
                //self.arrayItems.remove(at: indexPath.row)
                
                /*let realm = try! Realm()
                let objs = realm.objects(Booking.self)
                try! realm.write {
                    for obj in objs {
                        if obj.id == pfobj.id {
                            realm.delete(obj)
                        }
                    }
                }*/
                
                let banner = Banner(title: "Notifica", subtitle: "Sto cancellando la prenotazione", image: nil, backgroundColor: UIColor(red:255/255.0, green:59/255.0, blue:48/255.0, alpha:0.9))
                banner.dismissesOnTap = true
                banner.show(duration: 1)
                
                /*DispatchQueue.main.async {
                    ParseBridge().syncBooking(completion: { (success) in
                        let realm = try! Realm()
                        let result = realm.objects(Booking.self)
                        try! realm.write {
                            realm.delete(result)
                        }
                        self.queryRealm()
                    })
                }*/
                
                
                
            })
            
            
            
            self.queryRealm()
        }
        more.backgroundColor = UIColor.init(red: 255/255, green: 59/255, blue: 48/255, alpha: 2)
        
        let favorite = UITableViewRowAction(style: .normal, title: "Evento") { action, index in
            
            let pfObj = self.arrayItems[indexPath.row]
            let nsdate = pfObj.date
            
            let alertController = UIAlertController(title: "Aggiungo a calendario", message:"" , preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "Annulla", style: .destructive) { (action) in
                
            }
            
            let confirmAction = UIAlertAction(title: "Conferma", style: UIAlertActionStyle.cancel, handler: {
                (action : UIAlertAction!) -> Void in
                
                let firstTextField = alertController.textFields![0] as UITextField
                
                self.addEventToCalendar(title: firstTextField.text!, description: "f", startDate: nsdate as NSDate, endDate: nsdate as NSDate)
                
                let banner = Banner(title: "Notifica", subtitle: "Sto aggiungendo l'evento al calendario", image: nil, backgroundColor: UIColor(red:9/255.0, green:152/255.0, blue:232/255.0, alpha:0.9))
                banner.dismissesOnTap = true
                banner.show(duration: 1)
                
            })
            
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Title"
                textField.textAlignment = .center
                //textField.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
                textField.layer.cornerRadius = 5
                textField.borderStyle = .none
                textField.layer.borderWidth = 0
            }
            
            
            alertController.addAction(cancelAction)
            alertController.addAction(confirmAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        favorite.backgroundColor = UIColor.init(red: 9/255, green: 152/255, blue: 232/255, alpha: 2)
        
        
        return [favorite, more]
            
        } else {
            
            
            let favorite = UITableViewRowAction(style: .normal, title: "Evento") { action, index in
                
                let pfObj = self.arrayItems[indexPath.row]
                let nsdate = pfObj.date
                
                let alertController = UIAlertController(title: "Aggiungo a calendario", message:"" , preferredStyle: UIAlertControllerStyle.alert)
                
                let cancelAction = UIAlertAction(title: "Annulla", style: .destructive) { (action) in
                    
                }
                
                let confirmAction = UIAlertAction(title: "Conferma", style: UIAlertActionStyle.cancel, handler: {
                    (action : UIAlertAction!) -> Void in
                    
                    let firstTextField = alertController.textFields![0] as UITextField
                    
                    self.addEventToCalendar(title: firstTextField.text!, description: "f", startDate: nsdate as NSDate, endDate: nsdate as NSDate)
                    
                    let banner = Banner(title: "Notifica", subtitle: "Sto aggiungendo l'evento al calendario", image: nil, backgroundColor: UIColor(red:9/255.0, green:152/255.0, blue:232/255.0, alpha:0.9))
                    banner.dismissesOnTap = true
                    banner.show(duration: 1)
                    
                })
                
                alertController.addTextField { (textField : UITextField!) -> Void in
                    textField.placeholder = "Title"
                    textField.textAlignment = .center
                    //textField.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
                    textField.layer.cornerRadius = 5
                    textField.borderStyle = .none
                    textField.layer.borderWidth = 0
                }
                
                
                alertController.addAction(cancelAction)
                alertController.addAction(confirmAction)
                self.present(alertController, animated: true, completion: nil)
                
            }
            favorite.backgroundColor = UIColor.init(red: 9/255, green: 152/255, blue: 232/255, alpha: 2)
            
            
            return [favorite]
            
        }
    }
    
    func addEventToCalendar(title: String, description: String?, startDate: NSDate, endDate: NSDate, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate as Date
                event.endDate = endDate as Date
                event.notes = description
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let e as NSError {
                    completion?(false, e)
                    return
                }
                completion?(true, nil)
            } else {
                completion?(false, error as NSError?)
            }
        })
    }
}
