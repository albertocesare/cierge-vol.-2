//
//  infoTableViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 05/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import MapKit
import ParseUI
import RealmSwift
import Material
import BRYXBanner

class infoTableViewController: UITableViewController, MKMapViewDelegate {

    @IBOutlet weak var foto: PFImageView!
    @IBOutlet weak var titolo: UILabel!
    @IBOutlet weak var mapIcon: UIImageView!
    @IBOutlet weak var indirizzo: UILabel!
    @IBOutlet weak var btCheckin: UIButton!
    @IBOutlet weak var descrizione: UITextView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var btCall: UIButton!
    @IBOutlet weak var btMap: UIButton!
    
    var phoneNumber = ""
    var coordsBt = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        btMap.setImage(UIImage(named: "ic_place_white")?.maskWith(color: #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)), for: .normal)
        btCall.setImage(UIImage(named: "ic_phone_white")?.maskWith(color: #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)), for: .normal)
        
        label1.font = RobotoFont.medium
        label2.font = RobotoFont.medium

        self.navigationController?.hidesBottomBarWhenPushed = true
        mapView.isUserInteractionEnabled = false
        
        mapIcon.image = UIImage(named: "ic_place_white.png")?.maskWith(color: #colorLiteral(red: 0.8683364987, green: 0.7855109572, blue: 0.5089007616, alpha: 1))
        
        query()
        
        self.refreshControl?.addTarget(self, action: #selector(self.query), for: .valueChanged)
        
    }
    
    func query() {
    
        
        let user = UserDefaults.standard
        let idCheck = user.value(forKey: "idHotel")
        let iddCheck = user.string(forKey: "idCheck")
        
        if iddCheck != "none" {
            btCheckin.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            btCheckin.setTitle("Richiesta già inviata", for: .normal)
            btCheckin.setTitleColor(#colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1), for: .normal)
            btCheckin.isUserInteractionEnabled = false
        }
        
        let query = PFQuery(className: "Hotels")
        query.whereKey("objectId", equalTo: idCheck!)
        query.findObjectsInBackground(block: { (objects, error) in
            
            let hotel = objects?.first
            let foto = hotel?["photo"] as? PFFile
            foto?.getDataInBackground { (imageData, error) -> Void in
                
                let imagePhoto = UIImage(data:imageData!)
                self.foto.image = imagePhoto
                
            }
            
            self.descrizione.text = hotel?["description"] as! String
            
            
        })
        
        let realm = try! Realm()
        let hotels = realm.objects(Hotel.self)
        for hotel in hotels {
            
            if (hotel.id == idCheck as! String) {
                titolo.text = hotel.name
                
                indirizzo.text = hotel.address + ", " + hotel.city
                //descrizione.text = hotel.descr
                phoneNumber = hotel.phone
                
                
                let latitude = Double(hotel.latitude)
                let longitude = Double(hotel.longitude)
                
                coordsBt = latitude.description + "," + longitude.description
                
                let dropPin = MKPointAnnotation()
                let coordinate2d : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                dropPin.coordinate = coordinate2d
                self.mapView.addAnnotation(dropPin)
                let regione = MKCoordinateRegion.init(center: coordinate2d, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
                self.mapView.setRegion(regione, animated: true)
                
                
            }
            
        }
        
        self.refreshControl?.endRefreshing()
        
    }
    
    
    @IBAction func sendCheckIn () {
        
        let banner = Banner(title: "Notifica", subtitle: "Sto inviando la richiesta", image: nil, backgroundColor: UIColor.init(red: 221/255, green: 199/255, blue: 134/255, alpha: 0.9))
        banner.dismissesOnTap = true
        banner.show(duration: 1)
        
        btCheckin.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        btCheckin.setTitle("Richiesta già inviata", for: .normal)
        btCheckin.setTitleColor(#colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1), for: .normal)
        btCheckin.isUserInteractionEnabled = false
        
        let user = UserDefaults.standard
        let hotelId = user.string(forKey: "idHotel")
        
        let obj = PFObject(className: "UserCheckin")
        obj["userId"] = PFUser.current()?.objectId
        obj["hotelId"] = hotelId
        obj["state"] = "p"
        obj["active"] = true
        
        obj.saveInBackground()
    }
    
    @IBAction func phone(_ sender: UIButton) {
        
        UIApplication.shared.open(NSURL(string:"telprompt:" + phoneNumber)! as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func loca(_ sender: UIButton) {
        
        
        UIApplication.shared.open(NSURL(string:"http://maps.apple.com/?ll=" + coordsBt + "&q=Hotel")! as URL, options: [:], completionHandler: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 7
    }

    
    /*override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        

        return cell
    }*/
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
