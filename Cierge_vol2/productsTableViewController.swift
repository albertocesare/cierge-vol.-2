//
//  productsTableViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 08/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import RealmSwift
import BRYXBanner

class productsTableViewController: UITableViewController {
    
    @IBOutlet weak var photo : UIImageView!
    @IBOutlet weak var subtitle : UILabel!
    @IBOutlet weak var name : UILabel!
    @IBOutlet weak var descr : UITextView!
    @IBOutlet weak var buttonBuy : UIButton!
    @IBOutlet weak var price : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let user = UserDefaults.standard
        let id = user.string(forKey: "idService")

        let realm = try! Realm()
        let products = realm.objects(Product.self)
        for product in products {
            if product.id == id {
                
                subtitle.text = product.info.components(separatedBy: "*").first
                name.text = product.name
                descr.text = product.info.components(separatedBy: "*").last
                price.text = product.price + " €"
                
                let query = PFQuery(className: "Products")
                query.whereKey("objectId", equalTo: id!)
                query.findObjectsInBackground(block: { (objects, error) in
                    
                    let foto = objects?.first?["photo"] as! PFFile
                    foto.getDataInBackground(block: { (imageData, error) in
                     
                        let imagePhoto = UIImage(data:imageData!)
                        self.photo.image = imagePhoto
                        
                    })
                    
                })
                
                
            }
        }
        
    }
    
    @IBAction func buy () {
        
        let alertController = UIAlertController(title: "Inserisci Quantità", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        let ritiraAction = UIAlertAction(title: "Ritira", style: UIAlertActionStyle.default, handler: {
            alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
            firstTextField.keyboardType = .numberPad
            
            let user = UserDefaults.standard
            let id = user.string(forKey: "idService")
            let idH = user.string(forKey: "idHotel")
            
            let pf = PFObject(className: "Purchasing")
            pf["method"] = "p"
            pf["userId"] = PFUser.current()?.objectId
            pf["productId"] = id
            pf["qty"] = firstTextField.text
            pf["sellerId"] = idH
            pf["state"] = "p"
            pf.saveInBackground()
            
            let banner = Banner(title: "Notifica", subtitle: "Sto inviando la richiesta", image: nil, backgroundColor: UIColor.init(red: 221/255, green: 199/255, blue: 134/255, alpha: 0.9))
            banner.dismissesOnTap = true
            banner.show(duration: 1)
            
        })
        
        let spedisciAction = UIAlertAction(title: "Spedisci", style: UIAlertActionStyle.default, handler: {
            alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
            firstTextField.keyboardType = .numberPad
            
            let user = UserDefaults.standard
            let id = user.string(forKey: "idService")
            let idH = user.string(forKey: "idHotel")
            
            let pf = PFObject(className: "Purchasing")
            pf["method"] = "d"
            pf["userId"] = PFUser.current()?.objectId
            pf["productId"] = id
            pf["qty"] = firstTextField.text
            pf["sellerId"] = idH
            pf["state"] = "p"
            pf.saveInBackground()
            
            let banner = Banner(title: "Notifica", subtitle: "Sto inviando la richiesta", image: nil, backgroundColor: UIColor.init(red: 221/255, green: 199/255, blue: 134/255, alpha: 0.9))
            banner.dismissesOnTap = true
            banner.show(duration: 1)
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancella", style: UIAlertActionStyle.destructive, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.text = "1"
            textField.textAlignment = .center
            textField.keyboardType = .numberPad
            //textField.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            textField.layer.cornerRadius = 5
            textField.borderStyle = .none
            textField.layer.borderWidth = 0
        }
        
        
        alertController.addAction(ritiraAction)
        alertController.addAction(spedisciAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func dismiss () {
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
