//
//  registerController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 04/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse

class registerController: UITableViewController {
    
    @IBOutlet weak var textEmail : UITextField!
    @IBOutlet weak var textPassword : UITextField!
    @IBOutlet weak var textConfirmPassword : UITextField!
    @IBOutlet weak var activity : UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }
    
    @IBAction func register () {
        
        if (textEmail.text?.isEmpty == false) && (textPassword.text?.isEmpty == false) && (textConfirmPassword.text?.isEmpty == false) {
            activity.startAnimating()
            let newUser = PFUser()
            
            newUser.username = textEmail.text
            newUser.password = textPassword.text
            newUser.email = textEmail.text
            
            newUser.signUpInBackground { (result, error) -> Void in
                if error == nil {
                    print("[PARSE] SignUp successfull")
                    
                    let query = PFUser.query()
                    let userId = (PFUser.current()?.objectId)!
                    query?.whereKey("objectId", equalTo: userId)
                    query?.findObjectsInBackground(block: { (user, error) in
                        
                        user?.first?["name"] = "Nome"
                        user?.first?["active"] = true
                        user?.first?["info"] = "female*Roma*01/01/1977*3921037788"
                        user?.first?["phone"] = "123456789"
                        user?.first?["surname"] = " Cognome"
                        
                        user?.first?.saveInBackground()
                        
                    })
                    
                    self.performSegue(withIdentifier: "segueHome", sender: self)
                    
                } else {
                    print(error!)
                    self.activity.stopAnimating()
                    let alertController = UIAlertController(title: "Attenzione", message: "Controllare campo email", preferredStyle: UIAlertControllerStyle.alert)
                    
                    let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
                        alert -> Void in
                        
                        
                    })
                    
                    alertController.addAction(saveAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                }
            }
        } else {
            let alertController = UIAlertController(title: "Attenzione", message: "Controllare campi", preferredStyle: UIAlertControllerStyle.alert)
            
            let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
                alert -> Void in
                
                
            })
            
            alertController.addAction(saveAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
