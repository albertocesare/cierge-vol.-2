//
//  ristoInTableViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 06/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import RealmSwift
import BRYXBanner

class ristoInTableViewController: UITableViewController {
    
    @IBOutlet weak var photo : UIImageView!
    @IBOutlet weak var subtitle : UILabel!
    @IBOutlet weak var name : UILabel!
    @IBOutlet weak var descr : UITextView!
    @IBOutlet weak var buttonBuy : UIButton!
    @IBOutlet weak var opening : UILabel!
    @IBOutlet weak var openingDays : UILabel!
    @IBOutlet weak var price : UILabel!
    @IBOutlet weak var speciality : UILabel!
    
    let datePicker : UIDatePicker = UIDatePicker()
    
    var arrProd : [String] = []
    
    var heightCell = 200
    
    @IBAction func dismiss () {
        dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let user = UserDefaults.standard
        let id = user.string(forKey: "idService")
        
        let realm = try! Realm()
        let objs = realm.objects(InServices.self)
        
        for obj in objs {
            if obj.id == id! {
                
                price.text = "Prezzo: " + obj.price + " €"
                speciality.text = "Specialità: " + obj.speciality
                
                subtitle.text = obj.subtitle
                name.text = obj.name
                
                
                
                let closedDay = obj.closedDay.components(separatedBy: "_")
                var stringaChiude = "Giorni di chiusura: "
                for day in closedDay {
                    if (day == "0") {
                        
                        stringaChiude = stringaChiude + "aperto tutti i giorni"
                        
                    } else if (day == "1") {
                        
                        stringaChiude = stringaChiude + "lunedì "
                        
                    } else if (day == "2") {
                        
                        stringaChiude = stringaChiude + "martedì "
                        
                    } else if (day == "3") {
                        
                        stringaChiude = stringaChiude + "mercoledì "
                        
                    } else if (day == "4") {
                        
                        stringaChiude = stringaChiude + "giovedì "
                        
                    } else if (day == "5") {
                        
                        stringaChiude = stringaChiude + "venerdì "
                        
                    } else if (day == "6") {
                        
                        stringaChiude = stringaChiude + "sabato "
                        
                    } else if (day == "7") {
                        
                        stringaChiude = stringaChiude + "domenica "
                        
                    }
                }
                openingDays.text = stringaChiude
                
                let orari = obj.orari.components(separatedBy: "_")
                if (orari[0] == "1") {
                    
                    //Un turno
                    opening.text = "Apertura: " + orari[1] + ", " + orari[2]
                    
                    let date = UserDefaults.standard
                    let idCheck = date.string(forKey: "idCheck")
                    let query = PFQuery(className: "UserCheckin")
                    query.whereKey("objectId", equalTo: idCheck!)
                    query.findObjectsInBackground(block: { (objects, error) in
                        
                        let checkIn = objects?.first?["checkIndate"] as! String
                        let checkOut = objects?.first?["checkOutDate"] as! String
                        let dateIn = checkIn + " " + orari[1]
                        let dateOut = checkOut + " " + orari[2]
                        
                        print(dateIn)
                        print(dateOut)
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
                        
                        self.datePicker.minimumDate = dateFormatter.date(from: dateIn)!
                        self.datePicker.maximumDate = dateFormatter.date(from: dateOut)!
                        
                    })
                    
                    /*let dateFormatter = DateFormatter()
                     dateFormatter.dateFormat = ""
                     self.datePicker.minimumDate*/
                    
                } else {
                    
                    //Due turni
                    opening.text = "Apertura: " + orari[1] + ", " + orari[2] + " - " + orari[3] + ", " + orari[4]
                    
                    
                    let date = UserDefaults.standard
                    let idCheck = date.string(forKey: "idCheck")
                    let query = PFQuery(className: "UserCheckin")
                    query.whereKey("objectId", equalTo: idCheck!)
                    query.findObjectsInBackground(block: { (objects, error) in
                        
                        let checkIn = objects?.first?["checkIndate"] as! String
                        let checkOut = objects?.first?["checkOutDate"] as! String
                        let dateIn = checkIn + " " + orari[1]
                        let dateOut = checkOut + " " + orari[4]
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
                        
                        self.datePicker.minimumDate = dateFormatter.date(from: dateIn)!
                        self.datePicker.maximumDate = dateFormatter.date(from: dateOut)!
                        
                    })
                    
                }
                
                descr.text = obj.descr
                
                
                let query = PFQuery(className: "InServices")
                query.whereKey("objectId", equalTo: id!)
                query.findObjectsInBackground(block: { (objects, error) in
                    
                    
                    
                    let obj = objects?.first
                    
                    let prodotti = (obj?["info"] as! String).components(separatedBy: "@").last?.components(separatedBy: "*")
                    var a = 0
                    for prodotto in prodotti! {
                        
                        if (a > 4) {
                            self.arrProd.append(prodotto)
                        }
                        
                        a = a + 1
                    }
                    
                    
                    
                    let userImageFile = obj?["photo"] as! PFFile
                    userImageFile.getDataInBackground { (imageData, error) -> Void in
                        
                        let imagePhoto = UIImage(data:imageData!)
                        self.photo.image = imagePhoto
                        
                    }
                })
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 9
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        /*if (indexPath.row == 0) {
            return 64
        } else if (indexPath.row == 1) {
            return 200
        } else if (indexPath.row == 2) {
            return 80
        } else if (indexPath.row == 3) {
            return 70
        } else if (indexPath.row == 4) {
            return 0
        } else if (indexPath.row == 5) {
            return 200
        } else if (indexPath.row == 6) {
            return 200
        } else {
            return 0
        }
        */
        
        if (indexPath.row == 0) {
            return 64
        } else if (indexPath.row == 1) {
            return 200
        } else if (indexPath.row == 2) {
            return 80
        } else if (indexPath.row == 3) {
            return 70
        } else if (indexPath.row == 4) {
            return 44
        } else if (indexPath.row == 5) {
            return 44
        } else if (indexPath.row == 6) {
            return 0
        } else if (indexPath.row == 7) {
            return 200
        } else if (indexPath.row == 8) {
            return 200
        } else {
            return 0
        }
        
    }
    
    @IBAction func buy () {
        
        let userDef = UserDefaults.standard
        
        let alertController = UIAlertController(title: "Inserisci Quantità", message: "\n\n\n\n\n\n\n\n\n\n", preferredStyle: UIAlertControllerStyle.alert)
        alertController.view.frame = CGRect(x: 0, y: 0, width: 300, height: 200)
        alertController.view.bounds = CGRect(x: 0, y: 0, width: 300, height: 200)
        self.datePicker.frame = CGRect(x: alertController.view.frame.width/2 - 135, y: 20, width: 250, height: 200)
        self.datePicker.datePickerMode = .dateAndTime
        alertController.view.addSubview(self.datePicker)
        
        let saveAction = UIAlertAction(title: "Conferma", style: UIAlertActionStyle.default, handler: {
            alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
            firstTextField.keyboardType = .numberPad
            
            
            let saving = PFObject(className: "InBooking")
            saving["userId"] = PFUser.current()?.objectId
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy*HH:mm"
            
            let stringdate = dateFormatter.string(from: self.datePicker.date)
            saving["date"] = stringdate
            saving["state"] = "p"
            
            
            let user = UserDefaults()
            let idHotel = user.string(forKey: "idHotel")
            let idService = user.string(forKey: "idService")
            
            saving["subServices"] = "3@" + firstTextField.text!
            saving["hotelId"] = idHotel
            saving["serviceId"] = idService
            
            saving.saveInBackground()
            
            let banner = Banner(title: "Notifica", subtitle: "Sto inviando la richiesta", image: nil, backgroundColor: UIColor.init(red: 221/255, green: 199/255, blue: 134/255, alpha: 0.9))
            banner.dismissesOnTap = true
            banner.show(duration: 1)
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancella", style: UIAlertActionStyle.destructive, handler: {
            (action : UIAlertAction!) -> Void in
            
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.text = "1"
            textField.textAlignment = .center
            textField.keyboardType = .numberPad
            //textField.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
            textField.layer.cornerRadius = 5
            textField.borderStyle = .none
            textField.layer.borderWidth = 0
        }
        
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
