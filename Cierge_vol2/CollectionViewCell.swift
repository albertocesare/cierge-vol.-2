//
//  CollectionViewCell.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 05/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import ParseUI

class CollectionViewCell: PFCollectionViewCell {

    @IBOutlet weak var imageService : UIImageView!
    @IBOutlet weak var labelTitle : UILabel!
    @IBOutlet weak var labelPrice : UILabel!

}
