//
//  Classes.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 04/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import Foundation
import RealmSwift

class Hotel: Object {
    
    dynamic var name = ""
    dynamic var id = ""
    dynamic var stars = 0
    dynamic var active = true
    dynamic var zip = 0
    dynamic var phone = ""
    dynamic var ownerId = ""
    dynamic var address = ""
    dynamic var email = ""
    dynamic var ciergeName = ""
    dynamic var descr = ""
    dynamic var city = ""
    dynamic var latitude = 0.01
    dynamic var longitude = 0.01

}

class Product: Object {
    
    dynamic var id = ""
    dynamic var active = true
    dynamic var price = ""
    dynamic var name = ""
    dynamic var isBookable = true
    dynamic var info = ""
    dynamic var isDeliverable = true
    dynamic var sellerId = ""
    
}

class Profile: Object {
    
    dynamic var city = ""
    dynamic var id = ""
    dynamic var nome = ""
    dynamic var email = ""
    dynamic var telefono = ""
    dynamic var dataNascita = ""
    dynamic var gender = ""
    
}

class InServices: Object {
    
    dynamic var id = ""
    dynamic var hotelId = ""
    dynamic var type = ""
    dynamic var name = ""
    dynamic var subtitle = ""
    dynamic var descr = ""
    dynamic var orari = ""
    dynamic var closedDay = ""
    dynamic var accessories = ""
    dynamic var active = true
    dynamic var price = ""
    dynamic var speciality = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
}

class OutServices: Object {
    
    dynamic var id = ""
    dynamic var hotelId = ""
    dynamic var type = ""
    dynamic var serviceId = ""
    dynamic var active = true
    dynamic var name = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
}

class RistoDescr: Object {
    
    dynamic var id = ""
    dynamic var hotelId = ""
    dynamic var serviceId = ""
    dynamic var descr = ""
    
}

class ActDescr: Object {
    
    dynamic var id = ""
    dynamic var hotelId = ""
    dynamic var serviceId = ""
    dynamic var descr = ""
    
}

class UpdatedAt: Object {
    
    dynamic var date = Date()
    
}

class PlacesDescr: Object {
    
    dynamic var id = ""
    dynamic var hotelId = ""
    dynamic var serviceId = ""
    dynamic var descr = ""
    
}

class Booking: Object {
    
    dynamic var id = ""
    dynamic var idHotel = ""
    dynamic var idUser = ""
    dynamic var date = Date()
    dynamic var name = ""
    dynamic var state = ""
    dynamic var type = ""
}

struct TableItem {
    let title: String
    let state: String
    let date: NSDate
}


