//
//  inServicesTableViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 05/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import ParseUI
import RealmSwift

class inServicesTableViewController: PFQueryTableViewController {
    
    var arrayPF : [PFObject] = []

    override func queryForTable() -> PFQuery<PFObject> {
        
        let query = PFQuery(className: "InServices")
        let user = UserDefaults.standard
        let idHotel = user.value(forKey: "idHotel")
        query.whereKey("hotelId", equalTo: idHotel as! String)
        query.whereKey("deleted", notEqualTo: true)
        query.whereKey("active", equalTo: true)
        let idCheck = user.string(forKey: "idCheck")
        
        if idCheck == "none" {
            tableView.allowsSelection = false
            self.perform(#selector(inServicesTableViewController.popUp), with: nil, afterDelay: 0.5)
        }
        
        self.refreshControl?.addTarget(self, action: #selector(self.synch), for: .valueChanged)
        
        ParseBridge().syncServices(completion: {(sucess) in
            
        })
        
        return query
        
    }
    
    func synch () {
        
        
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, object: PFObject?) -> PFTableViewCell? {
        
        arrayPF.append(object!)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MTableViewCell
        
        let userImageFile = object?["photo"] as! PFFile
        userImageFile.getDataInBackground { (imageData, error) -> Void in
            
            let imagePhoto = UIImage(data:imageData!)
            cell.imageService.image = imagePhoto
            cell.labelTitle.text = (object?["info"] as! String).components(separatedBy: "@").last?.components(separatedBy: "*").first
            
        }
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = UserDefaults.standard
        user.set(arrayPF[indexPath.row].objectId, forKey: "idService")
        
        let type = (arrayPF[indexPath.row]["info"] as! String).components(separatedBy: "@").first
        
        
        if type == "1" {
            self.performSegue(withIdentifier: "segueChecklist", sender: nil)
        } else if type == "2" {
            self.performSegue(withIdentifier: "segueList", sender: nil)
        } else if type == "3" {
            self.performSegue(withIdentifier: "segueRisto", sender: nil)
        } else if type == "4" {
            self.performSegue(withIdentifier: "segueGen", sender: nil)
        }
        
        
        
    }
    
    func popUp () {
        
        let alertController = UIAlertController(title: "Attenzione", message: "Puoi usufruire dei servizi solo dopo aver ricevuto conferma di check-in da parte dell'hotel", preferredStyle: UIAlertControllerStyle.alert)
        
        let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
            alert -> Void in
            
            
        })
        
        
        
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
    

}
