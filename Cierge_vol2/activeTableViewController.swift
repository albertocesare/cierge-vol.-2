//
//  activeTableViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 04/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import ParseUI
import RealmSwift

class activeTableViewController: PFQueryTableViewController {
    
    var arrayPF : [PFObject] = []

    override func queryForTable() -> PFQuery<PFObject> {
        tableView.tableFooterView = UIView()
        let query = PFQuery(className: "UserCheckin")
        let userId = PFUser.current()?.objectId
        
        //query.cachePolicy = .networkElseCache
        query.whereKey("userId", equalTo: userId!)
        let arr = ["a","d"]
        query.whereKey("state", containedIn: arr)
        
        let queryy = PFQuery(className: "UserCheckin")
        queryy.whereKey("userId", equalTo: userId!)
        queryy.whereKey("state", containedIn: arr)
        queryy.countObjectsInBackground()
            {(count, error) in
                
                if (count < 1) {
                    
                    let textView : UITextView = UITextView()
                    textView.frame = CGRect(x: 20, y: self.view.frame.height / 2 - 50, width: self.view.frame.width - 40, height: 100)
                    textView.textAlignment = .center
                    textView.text = "In questa pagina visualizzerai le prenotazioni future raggruppate in base ai futuri soggiorni"
                    textView.font = UIFont.systemFont(ofSize: 15)
                    textView.textColor = .darkGray
                    self.tableView.tableFooterView?.addSubview(textView)
                }
        }
        
        return query
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, object: PFObject?) -> PFTableViewCell? {
        arrayPF.append(object!)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TTableViewCell
        
        if object?["state"] as! String == "d" {
            cell.buttonColor.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        } else if object?["state"] as! String == "a" {
            cell.buttonColor.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            cell.labelSubtitle.text = "In: " + (object?["checkIndate"] as! String) + " - Out: " + (object?["checkOutDate"] as! String)
        }
        
        let realm = try! Realm()
        let hotels = realm.objects(Hotel.self)
        for hotel in hotels {
            if (hotel.id == object?["hotelId"] as! String) {
                
                let name = hotel.name
                cell.labelTitle.text = name
                
                
                let arrName = name.components(separatedBy: " ")
                if arrName.count == 1 {
                    cell.buttonColor.setTitle(name.characters.first?.description, for: .normal)
                } else {
                    cell.buttonColor.setTitle((arrName[0].characters.first?.description)! + (arrName[1].characters.first?.description)!, for: .normal)
                }
                
            }
        }
        
        
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if dateFormatter.date(from: arrayPF[indexPath.row]["checkOutDate"] as! String)! > Date() {
            let user = UserDefaults.standard
            user.set(arrayPF[indexPath.row].objectId, forKey: "idCheck")
            user.set(arrayPF[indexPath.row]["hotelId"] as! String, forKey: "idHotel")
            self.performSegue(withIdentifier: "segueDescription", sender: nil)
        } else {
            let user = UserDefaults.standard
            user.set("none", forKey: "idCheck")
            user.set(arrayPF[indexPath.row]["hotelId"] as! String, forKey: "idHotel")
            self.performSegue(withIdentifier: "segueDescription", sender: nil)
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}
