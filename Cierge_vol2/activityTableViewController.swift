//
//  activityTableViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 05/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import ParseUI
import RealmSwift

class activityTableViewController: PFQueryTableViewController {
    
    var arrayPF : [PFObject] = []
    
    override func queryForTable() -> PFQuery<PFObject> {
        
        let query = PFQuery(className: "Activities")
        query.whereKey("deleted", notEqualTo: true)
        query.whereKey("active", equalTo: true)
        let user = UserDefaults.standard
        let idHotel = user.string(forKey: "idHotel")
        
        let realm = try! Realm()
        let objs = realm.objects(OutServices.self)
        var arrayObjs : [String] = []
        for obj in objs {
            
            if (obj.type == "a") && (obj.hotelId == idHotel) {
                arrayObjs.append(obj.serviceId)
            }
            
        }
        
        let idCheck = user.string(forKey: "idCheck")
        
        if idCheck == "none" {
            tableView.allowsSelection = false
        }
        
        query.whereKey("objectId", containedIn: arrayObjs)
        
        return query
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, object: PFObject?) -> PFTableViewCell? {
        
        arrayPF.append(object!)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MTableViewCell
        
        let userImageFile = object?["photo"] as! PFFile
        userImageFile.getDataInBackground { (imageData, error) -> Void in
            
            let imagePhoto = UIImage(data:imageData!)
            cell.imageService.image = imagePhoto
            cell.labelTitle.text = (object?["name"] as! String)
            
        }
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = UserDefaults.standard
        user.set(arrayPF[indexPath.row].objectId, forKey: "idService")
        print(arrayPF[indexPath.row].objectId)
        let pfObj = arrayPF[indexPath.row]
        let type = (pfObj["subServices"] as! String).components(separatedBy: "@").first
        if type == "1" {
            self.performSegue(withIdentifier: "segueActCheck", sender: nil)
        } else {
            self.performSegue(withIdentifier: "segueActList", sender: nil)
        }
        
    }
    
}
