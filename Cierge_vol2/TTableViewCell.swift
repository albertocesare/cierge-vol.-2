//
//  TTableViewCell.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 04/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import ParseUI

class TTableViewCell: PFTableViewCell {

    @IBOutlet weak var buttonColor : UIButton!
    @IBOutlet weak var labelTitle : UILabel!
    @IBOutlet weak var labelSubtitle : UILabel!

}
