//
//  searchTableViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 04/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import RealmSwift

class searchTableViewController: UITableViewController, UISearchBarDelegate {

    @IBOutlet var searchBar: UITableView!
    
    var itemsArray : [String] = []
    
    var filterdItemsArray : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()

        let realm = try! Realm()
        let hotels = realm.objects(Hotel.self)
        for hotel in hotels {
            
            if (hotel.active == true) {
                itemsArray.append(hotel.name + "*" + hotel.id)
            }
            
            
        }
        
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return filterdItemsArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TTableViewCell

        let obj = filterdItemsArray[indexPath.row]
        cell.labelTitle.text = obj.components(separatedBy: "*").first
        
        let realm = try! Realm()
        let hotels = realm.objects(Hotel.self)
        for hotel in hotels {
            if (hotel.id == obj.components(separatedBy: "*").last) {
                cell.labelSubtitle.text = hotel.city
            }
        }
        
        let arrName = obj.components(separatedBy: " ")
        if arrName.count == 1 {
            cell.buttonColor.setTitle(obj.characters.first?.description, for: .normal)
        } else {
            cell.buttonColor.setTitle((arrName[0].characters.first?.description)! + (arrName[1].characters.first?.description)!, for: .normal)
        }
        
        

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = UserDefaults.standard
        let id = filterdItemsArray[indexPath.row].components(separatedBy: "*").last
        
        user.set(id, forKey: "idHotel")
        user.set("none", forKey: "idCheck")
        
        self.performSegue(withIdentifier: "segueDescription", sender: nil)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filterContentForSearchText(searchText: searchText)
        tableView.reloadData()
        
    }
    
    func filterContentForSearchText(searchText: String) {
        filterdItemsArray = itemsArray.filter { item in
            return item.lowercased().contains(searchText.lowercased())
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    

}
