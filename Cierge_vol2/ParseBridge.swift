
//
//  ParseBridge.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 04/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import Foundation
import Parse
import RealmSwift

class ParseBridge {
    
    let a = 0
    
    func syncDatabase(completionMain: @escaping (Bool) -> ()) {
        
        DispatchQueue.main.async {
            
            self.syncHotels(completion: { (successHotel) in
                
                if successHotel{
                    print("Hotels done")
                    self.syncProducts(completion: { (successProduct) in
                        
                        if successProduct {
                            print("Products done")
                            self.syncProfile(completion: { (successProfile) in
                                
                                if successProfile {
                                    print("Profile done")
                                    self.syncServices(completion: { (successServices) in
                                        
                                        if successServices {
                                            print("Services done")
                                            self.syncOutServices(completion: { (successOut) in
                                                
                                                if successOut {
                                                    
                                                    print("Out done")
                                                    self.syncRestDescr(completion: { (successRest) in
                                                        
                                                        if successRest {
                                                            
                                                            print("OutRest Done")
                                                            self.syncActivDescr(completion: { (successActiv) in
                                                                
                                                                if successActiv {
                                                                    
                                                                    print("OutAct Done")
                                                                    self.syncPlacDescr(completion: { (successPla) in
                                                                        
                                                                        if successPla {
                                                                            
                                                                            print("OutPlaces Done")
                                                                            self.getNameInServices(completion: { (successInName) in
                                                                                
                                                                                if successInName {
                                                                                    print("NameIn done")
                                                                                    self.getNameOutServices(completion: { (successOutName) in
                                                                                        
                                                                                        if successOutName {
                                                                                            print("NameOut done")
                                                                                            
                                                                                            self.syncBooking(completion: { (successBooking) in
                                                                                                
                                                                                                if successBooking {
                                                                                                    print("booking Done")
                                                                                                    completionMain(true)
                                                                                                    
                                                                                                }
                                                                                                
                                                                                            })
                                                                                        }
                                                                                        
                                                                                    })
                                                                                }
                                                                                
                                                                            })
                                                                            
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                    
                                                }
                                                
                                            })
                                        }
                                        
                                    })
                                }
                                
                            })
                        }
                        
                    })
                }
                
            })
            
        }
        
    }
    
    func getNameInServices(completion: @escaping (Bool) -> Void) {
        
        DispatchQueue.main.async {
            
            
            let realm = try! Realm()
            let inServices = realm.objects(InServices.self)
            
            /*try! realm.safeWrite {
             let objPrec = realm.objects(InServices.self)
             realm.delete(objPrec)
             }*/
            
            for service in inServices {
                let query = PFQuery(className: "InServices")
                query.whereKey("objectId", equalTo: service.id)
                query.findObjectsInBackground(block: { (object, error) in
                    
                    try! realm.safeWrite {
                        let oS = OutServices()
                        oS.active = service.active
                        oS.hotelId = service.hotelId
                        oS.id = service.id
                        oS.name = ((object?.first?["info"] as! String).components(separatedBy: "@").last?.components(separatedBy: "*").first)!
                        oS.serviceId = service.id
                        oS.type = service.type
                        realm.add(oS, update: true)
                        print(oS.name)
                    }
                    
                })
                
                if service == inServices.last {
                    completion(true)
                }
            }
            
        }
        
    }
    
    func getNameOutServices(completion: @escaping (Bool) -> Void) {
        
        DispatchQueue.main.async {
            
            let realm = try! Realm()
            let outServices = realm.objects(OutServices.self)
            
            /*try! realm.safeWrite {
             let objPrec = realm.objects(OutServices.self)
             realm.delete(objPrec)
             }*/
            
            for service in outServices {
                
                if service.type == "a" {
                    
                    let query = PFQuery(className: "Activities")
                    query.whereKey("objectId", equalTo: service.serviceId)
                    query.findObjectsInBackground(block: { (object, error) in
                        
                        try! realm.safeWrite {
                            let oS = OutServices()
                            oS.active = service.active
                            oS.hotelId = service.hotelId
                            oS.id = service.id
                            oS.name = object?.first?["name"] as! String
                            oS.serviceId = service.serviceId
                            oS.type = service.type
                            realm.add(oS, update: true)
                        }
                        
                    })
                    
                } else if service.type == "r" {
                    
                    let query = PFQuery(className: "Restaurants")
                    print(service.id)
                    query.whereKey("objectId", equalTo: service.serviceId)
                    query.findObjectsInBackground(block: { (object, error) in
                        
                        try! realm.safeWrite {
                            let oS = OutServices()
                            oS.active = service.active
                            oS.hotelId = service.hotelId
                            oS.id = service.id
                            oS.name = object?.first?["name"] as! String
                            oS.serviceId = service.serviceId
                            oS.type = service.type
                            realm.add(oS, update: true)
                        }
                        
                        
                    })
                    
                }
                
                if service == outServices.last {
                    completion(true)
                }
            }
            
        }
        
    }
    
    func syncBooking(completion: @escaping (Bool) -> Void) {
        
        DispatchQueue.main.async {
            
            let realm = try! Realm()
            let results = realm.objects(Booking.self)
            try! realm.write {
                realm.delete(results)
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy*HH:mm"
            
            let queryActiv = PFQuery(className: "ActivBooking")
            let queryIn = PFQuery(className: "InBooking")
            let queryRisto = PFQuery(className: "RestBooking")
            
            if let userId = PFUser.current()?.objectId {
                
                queryActiv.whereKey("userId", equalTo: userId)
                queryActiv.findObjectsInBackground(block: { (objects, error) in
                    
                    for object in objects! {
                        
                        let realm = try! Realm()
                        try! realm.safeWrite {
                            
                            let book = Booking()
                            
                            //book.name = subobject?.first?["name"] as! String
                            book.id = object.objectId!
                            book.idHotel = object["hotelId"] as! String
                            book.date = dateFormatter.date(from: object["date"] as! String)!
                            book.idUser = object["userId"] as! String
                            book.state = object["state"] as! String
                            book.type = "act"
                            
                            let activities = realm.objects(OutServices.self)
                            print(activities)
                            for activity in activities {
                                if (activity.type == "a") && (activity.id == object["activityId"] as! String) {
                                    book.name = activity.name
                                    
                                }
                            }
                            
                            realm.add(book)
                        }
                        
                        
                    }
                    
                    queryIn.whereKey("userId", equalTo: (PFUser.current()?.objectId)!)
                    queryIn.findObjectsInBackground(block: { (objectss, error) in
                        
                        for object in objectss! {
                            
                            let realm = try! Realm()
                            try! realm.safeWrite {
                                let bookk = Booking()
                                bookk.id = object.objectId!
                                bookk.idHotel = object["hotelId"] as! String
                                bookk.date = dateFormatter.date(from: object["date"] as! String)!
                                bookk.idUser = object["userId"] as! String
                                bookk.state = object["state"] as! String
                                bookk.type = "ins"
                                
                                let activities = realm.objects(InServices.self)
                                for activity in activities {
                                    if (activity.id == object["serviceId"] as! String) {
                                        bookk.name = activity.name
                                        
                                    }
                                }
                                
                                realm.add(bookk)
                                
                            }
                            
                        }
                        
                        queryRisto.whereKey("userId", equalTo: (PFUser.current()?.objectId)!)
                        queryRisto.findObjectsInBackground(block: { (objectsss, error) in
                            
                            for object in objectsss! {
                                
                                let realm = try! Realm()
                                try! realm.safeWrite {
                                    let bookk = Booking()
                                    bookk.id = object.objectId!
                                    bookk.idHotel = object["hotelId"] as! String
                                    bookk.date = dateFormatter.date(from: object["date"] as! String)!
                                    bookk.idUser = object["userId"] as! String
                                    bookk.state = object["state"] as! String
                                    bookk.type = "rist"
                                    
                                    let activities = realm.objects(OutServices.self)
                                    for activity in activities {
                                        if (activity.id == object["restaurantId"] as! String) {
                                            bookk.name = activity.name
                                            
                                        }
                                    }
                                    
                                    
                                    realm.add(bookk)
                                    
                                }
                                
                            }
                            
                            completion(true)
                        })
                        
                        
                        
                        
                        
                    })
                })
                
            } else {
                completion(true)
            }
            
        }
        
    }
    
    func syncProducts(completion: @escaping (Bool) -> Void) {
        
        DispatchQueue.main.async {
            
            let realm = try! Realm()
            try! realm.safeWrite {
                
                let objPrec = realm.objects(Booking.self)
                realm.delete(objPrec)
                
            }
            
            let dispatchTime = DispatchTime.now() + .milliseconds(500)
            DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
                
                let query = PFQuery(className: "Products")
                query.whereKey("active", equalTo: true)
                query.whereKey("deleted", notEqualTo: true)
                query.findObjectsInBackground(block: { (objects, error) in
                    
                    let realm = try! Realm()
                    try! realm.write {
                        
                        let objPrec = realm.objects(Product.self)
                        realm.delete(objPrec)
                        
                        for product in objects! {
                            let realm = try! Realm()
                            try! realm.safeWrite {
                                
                                let p = Product()
                                p.id = product.objectId!
                                if let s = product["active"] as? Bool {
                                    p.active = s
                                }
                                if let s = product["price"] as? String {
                                    p.price = s
                                }
                                if let s = product["name"] as? String {
                                    p.name = s
                                }
                                if let s = product["isBookable"] as? Bool {
                                    p.isBookable = s
                                }
                                if let s = product["info"] as? String {
                                    p.info = s
                                }
                                if let s = product["isDeliverable"] as? Bool {
                                    p.isDeliverable = s
                                }
                                if let s = product["sellerId"] as? String {
                                    p.sellerId = s
                                }
                                
                                realm.add(p)
                            }
                            if (product == objects!.last) {
                                completion(true)
                            }
                        }
                        
                    }
                    
                })
                
            }
            
        }
        
    }
    
    func syncProfile(completion: @escaping (Bool) -> Void) {
        
        DispatchQueue.main.async {
            
            if (PFUser.current()?.objectId != nil) {
                
                //let query = PFQuery(className: "_User")
                let query = PFUser.query()
                let userId = (PFUser.current()?.objectId)!
                query?.whereKey("objectId", equalTo: userId)
                query?.findObjectsInBackground(block: { (objects, error) in
                    
                    let user = objects?.first
                    
                    let realm = try! Realm()
                    try! realm.safeWrite {
                        
                        let objPrec = realm.objects(Profile.self)
                        realm.delete(objPrec)
                        
                        let u = Profile()
                        u.id = userId
                        u.nome = user?["name"] as! String
                        let info = (user?["info"] as! String).components(separatedBy: "*")
                        let gender = info[0]
                        let city = info[1]
                        let dataNascita = info[2]
                        let numero = info[3]
                        
                        u.gender = gender
                        u.city = city
                        u.dataNascita = dataNascita
                        u.telefono = numero
                        u.email = user?["email"] as! String
                        
                        realm.add(u)
                        
                        
                    }
                    
                    
                })
                
                
            } else {
                completion(true)
            }
            
        }
    }
    
    func syncServices(completion: @escaping (Bool) -> Void) {
        
        DispatchQueue.main.async {
            
            let query = PFQuery(className: "InServices")
            query.whereKey("active", equalTo: true)
            query.whereKey("deleted", notEqualTo: true)
            query.findObjectsInBackground(block: { (objects, error) in
                
                let realm = try! Realm()
                try! realm.write {
                    
                    let objPrec = realm.objects(InServices.self)
                    realm.delete(objPrec)
                    
                }
                
                for service in objects! {
                    
                    let realm = try! Realm()
                    try! realm.safeWrite {
                        
                        print(service)
                        
                        let iS = InServices()
                        iS.id = service.objectId!
                        iS.active = service["active"] as! Bool
                        iS.hotelId = service["hotelId"] as! String
                        
                        let info = (service["info"] as! String).components(separatedBy: "@")
                        iS.type = info[0]
                        let subInfo = info[1].components(separatedBy: "*")
                        
                        iS.name = subInfo[0]
                        iS.subtitle = subInfo[1]
                        iS.descr = subInfo[2]
                        if (subInfo.count > 3) {
                            
                            iS.orari = subInfo[3]
                            iS.closedDay = subInfo[4]
                            iS.accessories = subInfo[5]
                            
                        }
                        
                        if info[0] == "3" {
                            iS.price = subInfo.last!
                            let count = subInfo.count
                            iS.speciality = subInfo[count - 2]
                        }
                        
                        realm.add(iS)
                    }
                    
                    if service == objects!.last {
                        completion(true)
                    }
                    
                }
                
                
                
            })
            
        }
        
        
    }
    
    func syncHotels(completion: @escaping (Bool) -> Void) {
        
        DispatchQueue.main.async {
            
            let query = PFQuery(className: "Hotels")
            query.findObjectsInBackground(block: { (objects, error) in
                
                let realm = try! Realm()
                try! realm.write {
                    
                    let objPrec = realm.objects(Hotel.self)
                    realm.delete(objPrec)
                    
                }
                
                for hotel in objects! {
                    
                    let realm = try! Realm()
                    try! realm.write {
                        
                        
                        let h = Hotel()
                        h.name = hotel["name"] as! String
                        h.id = hotel.objectId!
                        h.active = hotel["active"] as! Bool
                        let coords = hotel["coords"] as! String
                        h.latitude = Double(coords.components(separatedBy: "_")[0])!
                        h.longitude = Double(coords.components(separatedBy: "_")[1])!
                        
                        if let s = hotel["ownerId"] as? String {
                            h.ownerId = s
                        }
                        if let s = hotel["address"] as? String {
                            h.address = s
                        }
                        if let s = hotel["email"] as? String {
                            h.email = s
                        }
                        if let s = hotel["ciergeName"] as? String {
                            h.ciergeName = s
                        }
                        if let s = hotel["description"] as? String {
                            h.descr = s
                        }
                        if let s = hotel["phone"] as? String {
                            h.phone = s
                        }
                        if let s = hotel["city"] as? String {
                            h.city = s
                        }
                        
                        realm.add(h)
                    }
                    
                    if (hotel == objects!.last) {
                        completion(true)
                    }
                    
                }
                
                
            })
            
        }
        
    }
    
    func syncOutServices(completion: @escaping (Bool) -> Void) {
        
        DispatchQueue.main.async {
            
            let query = PFQuery(className: "OutServices")
            query.whereKey("active", equalTo: true)
            query.whereKey("deleted", notEqualTo: true)
            query.findObjectsInBackground(block: { (objects, error) in
                
                let realm = try! Realm()
                try! realm.write {
                    
                    let objPrec = realm.objects(OutServices.self)
                    realm.delete(objPrec)
                    
                }
                
                for service in objects! {
                    
                    let realm = try! Realm()
                    try! realm.safeWrite {
                        
                        let oS = OutServices()
                        oS.active = service["active"] as! Bool
                        oS.hotelId = service["hotelId"] as! String
                        oS.id = service.objectId!
                        oS.serviceId = service["serviceId"] as! String
                        oS.type = service["type"] as! String
                        
                        realm.add(oS)
                    }
                    
                    if service == objects!.last {
                        completion(true)
                    }
                    
                }
                
                
            })
            
        }
        
    }
    
    func syncActivDescr(completion: @escaping (Bool) -> Void) {
        
        DispatchQueue.main.async {
            
            let query = PFQuery(className: "ActivDescr")
            query.findObjectsInBackground(block: { (objects, error) in
                
                let realm = try! Realm()
                try! realm.write {
                    
                    let objPrec = realm.objects(ActDescr.self)
                    realm.delete(objPrec)
                    
                }
                
                for service in objects! {
                    
                    let realm = try! Realm()
                    try! realm.safeWrite {
                        
                        let oS = ActDescr()
                        oS.hotelId = service["hotelId"] as! String
                        oS.id = service.objectId!
                        oS.serviceId = service["activityId"] as! String
                        oS.descr = service["description"] as! String
                        
                        realm.add(oS)
                    }
                    
                    if service == objects!.last {
                        completion(true)
                    }
                    
                }
                
                
            })
            
        }
        
    }
    
    func syncPlacDescr(completion: @escaping (Bool) -> Void) {
        
        DispatchQueue.main.async {
            
            let query = PFQuery(className: "PlaceDescr")
            query.findObjectsInBackground(block: { (objects, error) in
                
                let realm = try! Realm()
                try! realm.write {
                    
                    let objPrec = realm.objects(PlacesDescr.self)
                    realm.delete(objPrec)
                    
                }
                
                for service in objects! {
                    
                    let realm = try! Realm()
                    try! realm.safeWrite {
                        
                        let oS = PlacesDescr()
                        oS.hotelId = service["hotelId"] as! String
                        oS.id = service.objectId!
                        oS.serviceId = service["placeId"] as! String
                        oS.descr = service["description"] as! String
                        
                        realm.add(oS)
                    }
                    
                    if service == objects!.last {
                        completion(true)
                    }
                    
                }
                
                
            })
            
        }
        
    }
    
    func syncRestDescr(completion: @escaping (Bool) -> Void) {
        
        DispatchQueue.main.async {
            
            let query = PFQuery(className: "RestDescr")
            query.findObjectsInBackground(block: { (objects, error) in
                
                let realm = try! Realm()
                try! realm.write {
                    
                    let objPrec = realm.objects(RistoDescr.self)
                    realm.delete(objPrec)
                    
                }
                
                for service in objects! {
                    
                    let realm = try! Realm()
                    try! realm.safeWrite {
                        
                        let oS = RistoDescr()
                        oS.hotelId = service["hotelId"] as! String
                        oS.id = service.objectId!
                        oS.serviceId = service["restaurantId"] as! String
                        oS.descr = service["description"] as! String
                        
                        realm.add(oS)
                    }
                    
                    if service == objects!.last {
                        completion(true)
                    }
                    
                }
                
                
            })
            
        }
        
    }
    
}

extension Realm {
    public func safeWrite(_ block: (() throws -> Void)) throws {
        if isInWriteTransaction {
            try block()
        } else {
            try write(block)
        }
    }
}
