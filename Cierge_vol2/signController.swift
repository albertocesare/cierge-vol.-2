//
//  signController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 04/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import NVActivityIndicatorView
import RealmSwift

class signController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userId = PFUser.current()?.objectId
        let sessionToken = PFUser.current()?.sessionToken
        
        
        let frame = CGRect(x: view.frame.width / 2 - 50, y: view.frame.height / 2 - 50, width: 100, height: 100)
        let activity = NVActivityIndicatorView(frame: frame, type: .pacman, color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), padding: CGFloat(0))
        view.addSubview(activity)
        activity.startAnimating()
        
        let realm = try! Realm()
        let calendar = Calendar.current
        let creatoI = (realm.objects(UpdatedAt.self).first)?.date
        
        if creatoI != nil {
            
            let add = calendar.date(byAdding: .day, value: 1, to: creatoI!)
            
            if add! < Date() {
                
                
                if (userId == nil) && (sessionToken == nil) {
                    DispatchQueue.main.async(){
                        try! realm.write {
                            let upd = UpdatedAt()
                            upd.date = Date()
                            realm.add(upd)
                        }
                        
                        self.perform(#selector(self.performSegueLogin), with: nil, afterDelay: 1)
                    }
                } else if ((userId != nil) && (sessionToken != nil)) {
                    DispatchQueue.main.async(){
                        try! realm.write {
                            let upd = UpdatedAt()
                            upd.date = Date()
                            realm.add(upd)
                        }
                        
                        self.perform(#selector(self.performSegueIntro), with: nil, afterDelay: 1)
                    }
                }
                
            } else {
                if (userId == nil) && (sessionToken == nil) {
                    DispatchQueue.main.async(){
                        
                        self.performSegue(withIdentifier: "segueLogin", sender: nil)
                        
                    }
                } else if ((userId != nil) && (sessionToken != nil)) {
                    DispatchQueue.main.async(){
                        
                        self.performSegue(withIdentifier: "segueIntro", sender: nil)
                        
                    }
                }
            }
            
        } else {
            
            if (userId == nil) && (sessionToken == nil) {
                DispatchQueue.main.async(){
                    try! realm.write {
                        let upd = UpdatedAt()
                        upd.date = Date()
                        realm.add(upd)
                    }
                    
                    self.perform(#selector(self.performSegueLogin), with: nil, afterDelay: 1)
                    
                }
            } else if ((userId != nil) && (sessionToken != nil)) {
                DispatchQueue.main.async(){
                    try! realm.write {
                        let upd = UpdatedAt()
                        upd.date = Date()
                        realm.add(upd)
                    }
                    
                    self.perform(#selector(self.performSegueIntro), with: nil, afterDelay: 1)
                }
            }
            
        }
        
        
        
        
    }
    
    func performSegueLogin() {
        DispatchQueue.main.async {
            ParseBridge().syncDatabase(completionMain: { (success) in
                
                self.performSegue(withIdentifier: "segueLogin", sender: nil)
                
            })
        }
        
    }
    
    func performSegueIntro() {
        DispatchQueue.main.async {
            ParseBridge().syncDatabase(completionMain: { (success) in
                
                self.performSegue(withIdentifier: "segueIntro", sender: nil)
                
            })
        }
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
