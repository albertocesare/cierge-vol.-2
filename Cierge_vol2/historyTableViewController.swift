//
//  historyTableViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 04/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import RealmSwift

class historyTableViewController: UITableViewController {
    
    var arrayCheckIn : [PFObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        
        let query = PFQuery(className: "UserCheckin")
        query.whereKey("state", equalTo: "a")
        query.whereKey("userId", equalTo: (PFUser.current()?.objectId)!)
        query.cachePolicy = .networkElseCache
        query.findObjectsInBackground(block: { (objects, error) in
            
            var a = 0
            for object in objects! {
                
                let dateFormatter = DateFormatter()
                dateFormatter.locale = NSLocale.current
                dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
                
                let datefromObj = dateFormatter.date(from: (object["checkIndate"] as! String) + " 01:59:59")
                
                if (datefromObj! < Date()) {
                    self.arrayCheckIn.append(object)
                    a = a + 1
                }
                self.tableView.reloadData()
            }
            if a == 0 {
                let textView : UITextView = UITextView()
                textView.frame = CGRect(x: 20, y: self.view.frame.height / 2 - 50, width: self.view.frame.width - 40, height: 100)
                textView.textAlignment = .center
                textView.text = "In questa pagina visualizzerai le prenotazioni future raggruppate in base ai futuri soggiorni"
                textView.font = UIFont.systemFont(ofSize: 15)
                textView.textColor = .darkGray
                self.tableView.tableFooterView?.addSubview(textView)
            }
            
            
            
        })
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arrayCheckIn.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let object = arrayCheckIn[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TTableViewCell
        
        let realm = try! Realm()
        let hotels = realm.objects(Hotel.self)
        for hotel in hotels {
            if (hotel.id == object["hotelId"] as! String) {
                
                let name = hotel.name
                cell.labelTitle.text = name
                
                
                let arrName = name.components(separatedBy: " ")
                if arrName.count == 1 {
                    cell.buttonColor.setTitle(name.characters.first?.description, for: .normal)
                } else {
                    cell.buttonColor.setTitle((arrName[0].characters.first?.description)! + (arrName[1].characters.first?.description)!, for: .normal)
                }
                
            }
        }
        
        cell.labelSubtitle.text = "In: " + (object["checkIndate"] as! String) + " - Out: " + (object["checkOutDate"] as! String)
        
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = UserDefaults.standard
        user.set(arrayCheckIn[indexPath.row]["hotelId"] as! String, forKey: "idHotel")
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "segueDetail", sender: nil)
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
