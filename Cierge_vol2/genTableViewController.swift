//
//  genTableViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 06/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import RealmSwift

class genTableViewController: UITableViewController {
    
    @IBOutlet weak var photo : UIImageView!
    @IBOutlet weak var subtitle : UILabel!
    @IBOutlet weak var name : UILabel!
    @IBOutlet weak var descr : UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let user = UserDefaults.standard
        let serviceId = user.string(forKey: "idService")

        let realm = try! Realm()
        let ins = realm.objects(InServices.self)
        for obj in ins {
            print(obj.id)
            print(serviceId)
            if obj.id == serviceId {
                
                let query = PFQuery(className: "InServices")
                query.whereKey("objectId", equalTo: obj.id)
                query.findObjectsInBackground(block: {(objects, error) in
                    
                    let photo = objects?.first?["photo"] as! PFFile
                    photo.getDataInBackground { (imageData, error) -> Void in
                        
                        let imagePhoto = UIImage(data:imageData!)
                        self.photo.image = imagePhoto
                        
                    }
                    
                })
                
                name.text = obj.name
                subtitle.text = obj.subtitle
                descr.text = obj.descr
                
                
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }
    
    @IBAction func dismiss() {
        dismiss(animated: true, completion: nil)
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
