//
//  actCheckProdTableViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 10/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse

class actCheckProdTableViewController: UITableViewController {
    
    var arrayProd : [String] = []
    
    var arraySelected : [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        let idService = UserDefaults.standard.string(forKey: "idService")
        
        let query = PFQuery(className: "Activities")
        query.whereKey("objectId", equalTo: idService!)
        query.findObjectsInBackground(block: { (objects, error) in
            
            let obj = objects?.first
            let info = (obj?["subServices"] as! String).components(separatedBy: "@").last?.components(separatedBy: "*")
            for inf in info! {
                
                if inf.components(separatedBy: "_").first == "a" {
                    self.arrayProd.append(inf)
                    print(inf)
                    self.tableView.reloadData()
                }
                
            }
            
        })
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arrayProd.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let info = arrayProd[indexPath.row]
        let name = info.components(separatedBy: "_")[1]
        let price = info.components(separatedBy: "_").last
        
        cell.textLabel?.text = name
        
        let priceLabel = UILabel(frame: CGRect(x: cell.frame.width - 100, y: cell.frame.height / 2 - 10, width: 60, height: 20))
        priceLabel.text = price! + " €"
        priceLabel.textAlignment = .right
        cell.addSubview(priceLabel)
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        
        let info = self.arrayProd[indexPath.row]
        let info1 = info.components(separatedBy: "_")[1]
        let info2 = info.components(separatedBy: "_").last!
        let stringObj = info1 + "_" + info2 + "_"
        
        var a = 0
        var b = 0
        for obj in arraySelected {
            if obj.contains(stringObj) {
                arraySelected.remove(at: a)
                cell?.accessoryType = .none
                b = 1
            } else {
                b = 0
            }
            a = a + 1
        }
        
        if b == 0 {
            
            tableView.deselectRow(at: indexPath, animated: true)
            
            let alertController = UIAlertController(title: "Inserisci Quantità", message: "", preferredStyle: UIAlertControllerStyle.alert)
            
            let saveAction = UIAlertAction(title: "Conferma", style: UIAlertActionStyle.default, handler: {
                alert -> Void in
                
                let firstTextField = alertController.textFields![0] as UITextField
                firstTextField.keyboardType = .numberPad
                
                let info = self.arrayProd[indexPath.row]
                let info1 = info.components(separatedBy: "_")[1]
                let info2 = info.components(separatedBy: "_").last!
                
                let stringObj = info1 + "_" + info2 + "_" + firstTextField.text!
                self.arraySelected.append(stringObj)
                
                cell?.accessoryType = .checkmark
                
                
                let userDef = UserDefaults.standard
                userDef.setValue(self.arraySelected, forKey: "arraySelected")
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancella", style: UIAlertActionStyle.destructive, handler: {
                (action : UIAlertAction!) -> Void in
                
                //tableView.deselectRow(at: indexPath, animated: true)
                tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = UITableViewCellAccessoryType.none
                
            })
            
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.text = "1"
                textField.textAlignment = .center
                textField.keyboardType = .numberPad
                //textField.backgroundColor = UIColor.init(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
                textField.layer.cornerRadius = 5
                textField.borderStyle = .none
                textField.layer.borderWidth = 0
            }
            
            
            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            let cell = tableView.cellForRow(at: indexPath)
            tableView.deselectRow(at: indexPath, animated: true)
            let info = self.arrayProd[indexPath.row]
            let info1 = info.components(separatedBy: "_")[1]
            let info2 = info.components(separatedBy: "_").last!
            let stringObj = info1 + "_" + info2 + "_"
            print(arraySelected)
            var a = 0
            for obj in arraySelected {
                if obj.contains(stringObj) {
                    arraySelected.remove(at: a)
                    cell?.accessoryType = .none
                }
                a = a + 1
            }
            let userDef = UserDefaults.standard
            userDef.setValue(self.arraySelected, forKey: "arraySelected")
            
        }
        
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
