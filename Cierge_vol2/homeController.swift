//
//  FirstViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 04/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import PageMenu

class homeController: UIViewController, CAPSPageMenuDelegate {

    var pageMenu : CAPSPageMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var controllerArray : [UIViewController] = []
        
        let controllerLogin : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "activeTableViewController") as! activeTableViewController
        controllerLogin.title = "Attivi"
        controllerArray.append(controllerLogin)
        
        let controllerRegister : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "pendingTableViewController") as! pendingTableViewController
        controllerRegister.title = "In Attesa"
        controllerArray.append(controllerRegister)
        
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(0),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .menuItemSeparatorWidth(0),
            .scrollMenuBackgroundColor(UIColor.clear),
            .viewBackgroundColor(UIColor.clear),
            .bottomMenuHairlineColor(UIColor.clear),
            .selectionIndicatorColor(#colorLiteral(red: 0.8683364987, green: 0.7855109572, blue: 0.5089007616, alpha: 1)),
            .menuMargin(20.0),
            .menuHeight(40.0),
            .selectedMenuItemLabelColor(#colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)),
            .unselectedMenuItemLabelColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)),
            .menuItemFont(UIFont(name: "HelveticaNeue-Medium", size: 14.0)!),
            ]
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0, y: 64, width: self.view.frame.width, height: view.frame.height - 64), pageMenuOptions: parameters)
        
        for view in (self.pageMenu?.view.subviews)! {
            if let scrollView = view as? UIScrollView
            {
                //scrollView.isScrollEnabled = false
            }
        }
        
        pageMenu!.delegate = self
        self.view.addSubview(pageMenu!.view)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        print(index)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
