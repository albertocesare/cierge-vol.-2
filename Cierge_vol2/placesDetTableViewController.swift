//
//  placesDetTableViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 10/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift
import Parse

class placesDetTableViewController: UITableViewController {

    @IBOutlet weak var photo : UIImageView!
    @IBOutlet weak var subtitle : UILabel!
    @IBOutlet weak var name : UILabel!
    @IBOutlet weak var opening : UILabel!
    @IBOutlet weak var openingDays : UILabel!
    @IBOutlet weak var ciergePhoto : UIImageView!
    @IBOutlet weak var descr : UITextView!
    @IBOutlet weak var buttonBuy : UIButton!
    @IBOutlet weak var labelCierge : UILabel!
    @IBOutlet weak var labelHotel : UILabel!
    @IBOutlet weak var btRaggiungi : UIButton!
    @IBOutlet weak var mapView : MKMapView!
    @IBOutlet weak var labelAdress : UILabel!
    
    var boolCierge = false
    var coordsBt = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.isUserInteractionEnabled = false
        
        btRaggiungi.setImage(UIImage(named: "ic_place_white")?.maskWith(color: #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)), for: .normal)
        //buttChiama.setImage(UIImage(named: "ic_phone_white")?.maskWith(color: #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)), for: .normal)
        //buttPrenota.setImage(UIImage(named: "cm_check_white")?.maskWith(color: #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)), for: .normal)
        
        let user = UserDefaults()
        let idService = user.string(forKey: "idService")
        print(idService)
        let idHotel = user.string(forKey: "idHotel")
        
        let realm = try! Realm()
        let rests = realm.objects(OutServices.self)
        let restsDescr = realm.objects(PlacesDescr.self)
        for rest in rests {
            
            if (rest.hotelId == idHotel) && (rest.serviceId == idService) {
                
                for restDescr in restsDescr {
                    
                    
                    if (restDescr.hotelId == idHotel) && (restDescr.serviceId == idService) {
                        //Descr Personalizzata
                        boolCierge = true
                        tableView.reloadData()
                        descr.text = restDescr.descr
                        let query = PFQuery(className: "Hotels")
                        query.whereKey("objectId", equalTo: idHotel!)
                        query.cachePolicy = .cacheElseNetwork
                        query.findObjectsInBackground(block: {(objs,error) in
                            
                            self.labelCierge.text = objs?.first?["ciergeName"] as? String
                            self.labelHotel.text = objs?.first?["name"] as? String
                            let foto = objs?.first?["ciergePhoto"] as? PFFile
                            foto?.getDataInBackground { (imageData, error) -> Void in
                                
                                let imagePhoto = UIImage(data:imageData!)
                                self.ciergePhoto.image = imagePhoto
                                
                            }
                            
                        })
                        
                    } else {
                        //No descr personalizzata
                    }
                    
                    
                    
                    
                }
                
                
                let query = PFQuery(className: "Places")
                query.whereKey("objectId", equalTo: idService!)
                query.findObjectsInBackground(block: {(objects, error) in
                    
                    self.subtitle.text = (objects?.first?["info"] as? String)?.components(separatedBy: "*")[3]
                    
                    if (self.boolCierge == false) {
                        //assegno la descr
                        self.descr.text = (objects?.first?["info"] as? String)?.components(separatedBy: "*")[2]
                        
                    }
                    
                    let foto = objects?.first?["photo"] as? PFFile
                    foto?.getDataInBackground { (imageData, error) -> Void in
                        
                        let imagePhoto = UIImage(data:imageData!)
                        self.photo.image = imagePhoto
                        
                    }
                    
                    
                    self.name.text = objects?.first?["name"] as? String
                    let closedDays = (objects?.first?["info"] as? String)?.components(separatedBy: "*")[1]
                    let orari = (objects?.first?["info"] as? String)?.components(separatedBy: "*").first
                    
                    self.returnOpeningDays(closedDays: closedDays!, orari: orari!)
                    print(objects?.first)
                    self.labelAdress.text = objects?.first?["city"] as? String
                    
                    let latitude = Double(((objects?.first?["coords"] as? String)?.components(separatedBy: "_").first)!)
                    let longitude = Double(((objects?.first?["coords"] as? String)?.components(separatedBy: "_").last)!)
                    self.coordsBt = (latitude?.description)! + "," + (longitude?.description)!
                    let dropPin = MKPointAnnotation()
                    let coordinate2d : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
                    dropPin.coordinate = coordinate2d
                    self.mapView.addAnnotation(dropPin)
                    let regione = MKCoordinateRegion.init(center: coordinate2d, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
                    self.mapView.setRegion(regione, animated: true)
                    
                    
                })
                
                
            }
            
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 9
    }
    
    @IBAction func dismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func raggiungi () {
        
        UIApplication.shared.open(NSURL(string:"http://maps.apple.com/?ll=" + coordsBt + "&q=Ristorante")! as URL, options: [:], completionHandler: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.row == 0) {
            return 64
        } else if (indexPath.row == 1) {
            return 200
        } else if (indexPath.row == 2) {
            return 80
        } else if (indexPath.row == 3) {
            return 70
        } else if (indexPath.row == 4) {
            return 44
        } else if (indexPath.row == 6) && (boolCierge == false) {
            return 0
        } else  if (indexPath.row == 6) && (boolCierge == true) {
            return 80
        } else if (indexPath.row == 5) {
            return 90
        } else if (indexPath.row == 7) {
            return 200
        } else if (indexPath.row == 8) {
            return 200
        } else {
            return 0
        }
        
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func returnOpeningDays(closedDays: String, orari: String) {
        
        let closedDay = closedDays.components(separatedBy: "_")
        var stringaChiude = "Giorni di chiusura: "
        for day in closedDay {
            if (day == "0") {
                
                stringaChiude = stringaChiude + "aperto tutti i giorni"
                
            } else if (day == "1") {
                
                stringaChiude = stringaChiude + "lun "
                
            } else if (day == "2") {
                
                stringaChiude = stringaChiude + "mar "
                
            } else if (day == "3") {
                
                stringaChiude = stringaChiude + "mer "
                
            } else if (day == "4") {
                
                stringaChiude = stringaChiude + "gio "
                
            } else if (day == "5") {
                
                stringaChiude = stringaChiude + "ven "
                
            } else if (day == "6") {
                
                stringaChiude = stringaChiude + "sab "
                
            } else if (day == "7") {
                
                stringaChiude = stringaChiude + "dom "
                
            }
        }
        openingDays.text = stringaChiude
        
        let orari = orari.components(separatedBy: "_")
        if orari.first != "0" {
        if (orari[0] == "1") {
            
            //Un turno
            opening.text = "Apertura: " + orari[1] + ", " + orari[2]
            
            
        } else {
            
            //Due turni
            opening.text = "Apertura: " + orari[1] + ", " + orari[2] + " - " + orari[3] + ", " + orari[4]
            
            
        }
        } else {
            opening.text = "Orario: 24/24h"
        }
        
    }

}
