//
//  productsCollectionViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 05/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import ParseUI
import Material

class productsCollectionViewController: PFQueryCollectionViewController {
    
    var arrayPF : [PFObject] = []

    override func queryForCollection() -> PFQuery<PFObject> {
        let query = PFQuery(className: "Products")
        query.whereKey("deleted", notEqualTo: true)
        query.whereKey("active", equalTo: true)
        
        let user = UserDefaults.standard
        let idHotel = user.string(forKey: "idHotel")
        
        query.whereKey("sellerId", equalTo: idHotel!)
        
        let idCheck = user.string(forKey: "idCheck")
        
        if idCheck == "none" {
            collectionView?.allowsSelection = false
        }
        
        return query
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, object: PFObject?) -> PFCollectionViewCell? {
        
        arrayPF.append(object!)
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? PFCollectionViewCell
        
        cell?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: (cell?.frame.width)!, height: (cell?.frame.height)! - 55))
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        let labelTitle = UILabel(frame: CGRect(x: 8, y: (cell?.frame.height)! - 50, width: (cell?.frame.width)! - 16, height: 25))
        let labelPrice = UILabel(frame: CGRect(x: 8, y: (cell?.frame.height)! - 20, width: (cell?.frame.width)! - 16, height: 17.5))
        
        labelTitle.font = RobotoFont.medium(with: 18)
        labelPrice.font = RobotoFont.regular(with: 15)
        
        labelTitle.textColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        labelTitle.adjustsFontSizeToFitWidth = true
        labelTitle.textAlignment = .center
        labelTitle.backgroundColor = .white
        labelPrice.textAlignment = .center
        labelPrice.adjustsFontSizeToFitWidth = true
        labelPrice.textColor = #colorLiteral(red: 0.5741485357, green: 0.5741624236, blue: 0.574154973, alpha: 1)
        labelPrice.backgroundColor = .white
        
        let userImageFile = object?["photo"] as! PFFile
        userImageFile.getDataInBackground { (imageData, error) -> Void in
            
            let imagePhoto = UIImage(data:imageData!)
            imageView.image = imagePhoto
            labelTitle.text = object?["name"] as? String
            labelPrice.text = (object?["price"] as? String)! + " €"
            
            /*let viewCell = UIView(frame: CGRect(x: -500, y: -500, width: 1000, height: 1000))
            viewCell.backgroundColor = .white
            cell?.addSubview(viewCell)*/
            
            cell?.addSubview(imageView)
            cell?.addSubview(labelTitle)
            cell?.addSubview(labelPrice)
            
            //cell?.textLabel.text = (object?["name"] as? String)! + "\n" + (object?["price"] as? String)! + " €"
            
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let user = UserDefaults.standard
        user.set(arrayPF[indexPath.row].objectId, forKey: "idService")
        collectionView.deselectItem(at: indexPath, animated: true)
        performSegue(withIdentifier: "segueProduct", sender: nil)
    }

}
