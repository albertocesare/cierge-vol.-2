//
//  settingsTableViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 05/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import RealmSwift
import BRYXBanner

class settingsTableViewController: UITableViewController, UIImagePickerControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var fotoProfilo : UIImageView!
    @IBOutlet weak var nomeField : UITextField!
    @IBOutlet weak var emailField : UITextField!
    @IBOutlet weak var numeroField : UITextField!
    @IBOutlet weak var dataPicker : UIDatePicker!
    @IBOutlet weak var genderControl : UISegmentedControl!
    @IBOutlet weak var fotoDocFronte : UIImageView!
    @IBOutlet weak var fotoDocRetro : UIImageView!
    @IBOutlet weak var citta : UITextField!
    
    var imagePicker: UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let viewfooter = UIView()
        viewfooter.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        tableView.tableFooterView = viewfooter
        
        
        let query = PFUser.query()
        query?.whereKey("objectId", equalTo: (PFUser.current()?.objectId)!)
        if (query?.hasCachedResult)! {
            query?.cachePolicy = .cacheThenNetwork
        }
        query?.findObjectsInBackground(block: { (objects, error) in
            
            let user = objects!.first
            if let userImageFile = user?["photo"] as? PFFile {
                userImageFile.getDataInBackground { (imageData, error) -> Void in
                    
                    let imagePhoto = UIImage(data:imageData!)
                    self.fotoProfilo.image = imagePhoto
                    
                }
            } else {self.fotoProfilo.image = #imageLiteral(resourceName: "nobody")}
            
            if let userImageFiledocback = user?["docBack"] as? PFFile {
                userImageFiledocback.getDataInBackground { (imageData, error) -> Void in
                    let imageDocRetro = UIImage(data:imageData!)
                    self.fotoDocRetro.image = imageDocRetro
                }
            } else {self.fotoDocRetro.image = #imageLiteral(resourceName: "Placeholder")}
            if let userImageFiledocfront = user?["docFront"] as? PFFile {
                userImageFiledocfront.getDataInBackground { (imageData, error) -> Void in
                    let imageDocFronte = UIImage(data:imageData!)
                    self.fotoDocFronte.image = imageDocFronte
                }
            } else {self.fotoDocFronte.image = #imageLiteral(resourceName: "Placeholder")}
            
            let queryy = PFUser.query()
            queryy?.whereKey("objectId", equalTo: (PFUser.current()?.objectId)!)
            query?.findObjectsInBackground(block: { (objects, error) in
                
                let user = objects!.first
                
                self.nomeField.text = (user?["name"] as! String) + " " + (user?["surname"] as! String)
                self.emailField.text = user?["email"] as! String
                if let phone = user?["phone"] as? String {
                    self.numeroField.text = phone
                }
                self.citta.text = (user?["info"] as! String).components(separatedBy: "*")[1]
                
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy"
                if let date = formatter.date(from: ((user?["info"] as! String).components(separatedBy: "*")[2]))
                {
                    self.dataPicker.date = date
                }
                //self.dataPicker.date = formatter.date(from: ((user?["info"] as! String).components(separatedBy: "*")[2]))!
                
                let gender = (user?["info"] as! String).components(separatedBy: "*")[0]
                let firstGender = gender.characters.first
                if firstGender == "m" {
                    self.genderControl.selectedSegmentIndex = 0
                } else {
                    self.genderControl.selectedSegmentIndex = 1
                }

                
            })
            
        })
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    var a = 0
    @IBAction func buttFront() {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        a = 1
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func buttBack() {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        a = 2
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func buttFoto() {
        
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        a = 3
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let user = PFUser.current()
        
        if (a == 1){
            
            fotoDocFronte.image = chosenImage
            let imageData = UIImagePNGRepresentation(chosenImage)
            let imageFile = PFFile(name:"back.png", data:imageData!)
            user?["docFront"] = imageFile
            user?.saveInBackground()
            
        } else if (a == 2) {
            
            fotoDocRetro.image = chosenImage
            let imageData = UIImagePNGRepresentation(chosenImage)
            let imageFile = PFFile(name:"front.png", data:imageData!)
            user?["docBack"] = imageFile
            user?.saveInBackground()
            
        } else {
            let imageTosave = cropImageToSquare(image: chosenImage)
            fotoProfilo.image = imageTosave
            let imageData = UIImagePNGRepresentation(imageTosave!)
            let imageFile = PFFile(name:"profile.png", data:imageData!)
            user?["photo"] = imageFile
            user?.saveInBackground()
        }
        
        a = 0
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func saveSettings () {
        
        if (numeroField.text != "") && (citta.text?.contains("*") == false) && (citta.text?.contains("@") == false) && (citta.text != "") {
            
            let query = PFQuery(className:"_User")
            query.getObjectInBackground(withId: (PFUser.current()?.objectId)!, block: { (object, error) -> Void in
                
                //gender paese dd/mm/yy phone
                var gender = ""
                if (self.genderControl.selectedSegmentIndex == 0) {
                    gender = "male"
                } else {
                    gender = "female"
                }
                
                let formatter = DateFormatter()
                let date = formatter.string(from: self.dataPicker.date)
                
                let infoString = gender + "*" + self.citta.text! + "*" + date + "*" + self.numeroField.text!
                
                object?.setValue(infoString, forKey: "info")
                
                object?.setValue(self.nomeField.text, forKey: "name")
                
                object?.saveInBackground()
                
                let banner = Banner(title: "Notifica", subtitle: "Sto inviando la richiesta", image: nil, backgroundColor: UIColor.init(red: 221/255, green: 199/255, blue: 134/255, alpha: 0.9))
                banner.dismissesOnTap = true
                banner.show(duration: 1)
                
                
            })
            
            
        }
        
    }
    
    @IBAction func logOut () {
        
        PFUser.logOut()
        //self.performSegue(withIdentifier: "segueLogOut", sender: nil)
        let controllerLogin : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "globalLoginController") as! globalLoginController
        present(controllerLogin, animated: true, completion: nil)
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 12
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func cropImageToSquare(image: UIImage) -> UIImage? {
        var imageHeight = image.size.height
        var imageWidth = image.size.width
        
        if imageHeight > imageWidth {
            imageHeight = imageWidth
        }
        else {
            imageWidth = imageHeight
        }
        
        let size = CGSize(width: imageWidth, height: imageHeight)
        
        let refWidth : CGFloat = CGFloat(image.cgImage!.width)
        let refHeight : CGFloat = CGFloat(image.cgImage!.height)
        
        let x = (refWidth - size.width) / 2
        let y = (refHeight - size.height) / 2
        
        let cropRect = CGRect(x: x, y: y, width: size.height, height: size.width)
        if let imageRef = image.cgImage!.cropping(to: cropRect) {
            return UIImage(cgImage: imageRef, scale: 0, orientation: image.imageOrientation)
        }
        
        return nil
    }
    
}
