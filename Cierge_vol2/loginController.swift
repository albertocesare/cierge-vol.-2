//
//  loginController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 04/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import PageMenu
import Parse

class loginController: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet weak var textEmail : UITextField!
    @IBOutlet weak var textPassword : UITextField!
    @IBOutlet weak var activity : UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        #if DEBUG
            textEmail.text = "albertocesare@alice.it"
            textPassword.text = "Smaile4u"
        #else
            
        #endif
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }
    
    
    @IBAction func login () {
        activity.startAnimating()
        if (textEmail.text?.isEmpty == false) && (textPassword.text?.isEmpty == false) {
            
            guard let username = self.textEmail.text else { return }
            guard let password = self.textPassword.text else { return }
            
            PFUser.logInWithUsername(inBackground: username, password: password) { (user, error) -> Void in
                if user != nil {
                    print("[PARSE] Login effettuato con successo")
                    self.performSegue(withIdentifier: "segueHome", sender: self)
                    self.perform(#selector(self.bridgeFunc), with: nil, afterDelay: 1)
                    
                } else {
                    
                    self.activity.stopAnimating()
                    let alertController = UIAlertController(title: "Attenzione", message: "Email o password sbagliate", preferredStyle: UIAlertControllerStyle.alert)
                    
                    let saveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
                        alert -> Void in
                        
                        
                    })
                    
                    alertController.addAction(saveAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                }
            }
            
            
            
            
            
            
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func bridgeFunc () {
        DispatchQueue.main.async(){
            self.synchUserData(completions: { (success) in
                
                if success {
                    self.synchUserData(completions: {(_) in
                        
                    })
                }
                
            })
        }
        
    }
    
    
    func synchUserData (completions: @escaping (Bool) -> Void) {
        DispatchQueue.main.async {
            ParseBridge().syncBooking(completion: { (successBooking) in
                
                if (successBooking) {
                    
                    print("booking done")
                    ParseBridge().syncProfile(completion: { (successProfile) in
                        
                        if (successProfile) {
                            
                            print("profile done")
                            completions(true)
                            
                        }
                        
                    })
                    
                }
                
            })
        }
        
    }
    
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
