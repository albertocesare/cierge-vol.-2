//
//  centralPagingViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 05/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import PageMenu

class centralPagingViewController: UIViewController, CAPSPageMenuDelegate {
    
    var pageMenu : CAPSPageMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        
        let bridge = ParseBridge()
        bridge.syncProfile(completion: {(success) in
            if success {
                print("Profile done")
            }
        })
        
        var controllerArray : [UIViewController] = []
        
        let controllerInfo : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "infoTableViewController") as! infoTableViewController
        controllerInfo.title = "Info"
        controllerArray.append(controllerInfo)
        
        let controllerInServices : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "inServicesTableViewController") as! inServicesTableViewController
        controllerInServices.title = "Servizi interni"
        controllerArray.append(controllerInServices)
        
        let controllerProducts : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "productsCollectionViewController") as! productsCollectionViewController
        controllerProducts.title = "Prodotti"
        controllerArray.append(controllerProducts)
        
        let controllerRisto : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ristoTableViewController") as! ristoTableViewController
        controllerRisto.title = "Ristoranti"
        controllerArray.append(controllerRisto)
        
        let controllerActivity : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "activityTableViewController") as! activityTableViewController
        controllerActivity.title = "Attività"
        controllerArray.append(controllerActivity)
        
        let controllerPlaces : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "placesTableViewController") as! placesTableViewController
        controllerPlaces.title = "Luoghi"
        controllerArray.append(controllerPlaces)
        
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(0),
            .useMenuLikeSegmentedControl(false),
            .menuItemSeparatorPercentageHeight(0.1),
            .menuItemSeparatorWidth(0),
            .scrollMenuBackgroundColor(UIColor.clear),
            .viewBackgroundColor(UIColor.clear),
            .bottomMenuHairlineColor(UIColor.clear),
            .selectionIndicatorColor(#colorLiteral(red: 0.8683364987, green: 0.7855109572, blue: 0.5089007616, alpha: 1)),
            .menuMargin(20.0),
            .menuHeight(40.0),
            .selectedMenuItemLabelColor(#colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)),
            .unselectedMenuItemLabelColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)),
            .menuItemFont(UIFont(name: "HelveticaNeue-Medium", size: 14.0)!),
            ]
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0, y: 64, width: self.view.frame.width, height: view.frame.height - 64), pageMenuOptions: parameters)
        
        pageMenu!.delegate = self
        self.view.addSubview(pageMenu!.view)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        print(index)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func dismiss() {
        dismiss(animated: true, completion: nil)
    }
    
}

extension UIImage {
    
    func maskWith(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: 0, y: size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.setBlendMode(.normal)
        
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        context.clip(to: rect, mask: cgImage!)
        
        color.setFill()
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return newImage
    }        
}
