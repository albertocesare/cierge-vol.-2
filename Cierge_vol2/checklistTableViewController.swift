//
//  checklistTableViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 06/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse
import RealmSwift
import BRYXBanner

class checklistTableViewController: UITableViewController {
    
    @IBOutlet weak var photo : UIImageView!
    @IBOutlet weak var subtitle : UILabel!
    @IBOutlet weak var name : UILabel!
    @IBOutlet weak var opening : UILabel!
    @IBOutlet weak var openingDays : UILabel!
    @IBOutlet weak var ciergePhoto : UIImageView!
    @IBOutlet weak var descr : UITextView!
    @IBOutlet weak var buttonBuy : UIButton!
    @IBOutlet weak var labelCierge : UILabel!
    @IBOutlet weak var labelHotel : UILabel!
    
    var datePicker : UIDatePicker = UIDatePicker()
    
    var arrProd : [String] = []
    var heightCell = 160
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let user = UserDefaults.standard
        let id = user.string(forKey: "idService")
        
        let realm = try! Realm()
        let objs = realm.objects(InServices.self)
        
        for obj in objs {
            if obj.id == id! {
                
                subtitle.text = obj.subtitle
                name.text = obj.name
                
                
                
                self.tableView.reloadData()
                
                let closedDay = obj.closedDay.components(separatedBy: "_")
                var stringaChiude = "Giorni di chiusura: "
                for day in closedDay {
                    if (day == "0") {
                        
                        stringaChiude = stringaChiude + "aperto tutti i giorni"
                        
                    } else if (day == "1") {
                        
                        stringaChiude = stringaChiude + "lunedì "
                    
                    } else if (day == "2") {
                        
                        stringaChiude = stringaChiude + "martedì "
                        
                    } else if (day == "3") {
                        
                        stringaChiude = stringaChiude + "mercoledì "
                        
                    } else if (day == "4") {
                        
                        stringaChiude = stringaChiude + "giovedì "
                        
                    } else if (day == "5") {
                        
                        stringaChiude = stringaChiude + "venerdì "
                        
                    } else if (day == "6") {
                        
                        stringaChiude = stringaChiude + "sabato "
                        
                    } else if (day == "7") {
                        
                        stringaChiude = stringaChiude + "domenica "
                        
                    }
                }
                openingDays.text = stringaChiude
                
                let orari = obj.orari.components(separatedBy: "_")
                if orari.first != "0" {
                if (orari[0] == "1") {
                    
                    //Un turno
                    opening.text = "Apertura: " + orari[1] + ", " + orari[2]
                    
                    let date = UserDefaults.standard
                    let idCheck = date.string(forKey: "idCheck")
                    print(idCheck)
                    let query = PFQuery(className: "UserCheckin")
                    query.whereKey("objectId", equalTo: idCheck!)
                    query.findObjectsInBackground(block: { (objects, error) in
                        
                        let checkIn = objects?.first?["checkIndate"] as! String
                        let checkOut = objects?.first?["checkOutDate"] as! String
                        let dateIn = checkIn + " " + orari[1]
                        let dateOut = checkOut + " " + orari[2]
                        
                        print(dateIn)
                        print(dateOut)
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
                        
                        
                        let dateInAdd = Calendar.current.date(byAdding: .hour, value: 2, to: dateFormatter.date(from: dateIn)!)
                        let dateOutAdd = Calendar.current.date(byAdding: .hour, value: 2, to: dateFormatter.date(from: dateOut)!)
                        
                        print(dateInAdd)
                        print(dateOutAdd)
                        
                        self.datePicker.minimumDate = dateFormatter.date(from: dateIn)!
                        self.datePicker.maximumDate = dateFormatter.date(from: dateOut)!
                        
                    })
                    
                    /*let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = ""
                    self.datePicker.minimumDate*/
                    
                } else {
                    
                    //Due turni
                    opening.text = "Apertura: " + orari[1] + ", " + orari[2] + " - " + orari[3] + ", " + orari[4]
                    
                }
                
                descr.text = obj.descr
                
                
                let query = PFQuery(className: "InServices")
                query.whereKey("objectId", equalTo: id!)
                query.findObjectsInBackground(block: { (objects, error) in
                    
                    
                    
                    let obj = objects?.first
                    
                    let prodotti = (obj?["info"] as! String).components(separatedBy: "@").last?.components(separatedBy: "*")
                    let count = (prodotti?.count)! - 4
                    self.heightCell = count * 80
                    self.tableView.reloadData()
                    var a = 0
                    for prodotto in prodotti! {
                        
                        if (a > 4) {
                            self.arrProd.append(prodotto)
                        }
                        
                        a = a + 1
                    }
                    
                    
                    
                    let userImageFile = obj?["photo"] as! PFFile
                    userImageFile.getDataInBackground { (imageData, error) -> Void in
                        
                        let imagePhoto = UIImage(data:imageData!)
                        self.photo.image = imagePhoto
                        
                    }
                })
                }
            } else {
                    opening.text = "Orario: 24/24h"
                }
        }
        
        
    }
    
    @IBAction func buy() {
        
        let user = UserDefaults.standard
        let array = user.array(forKey: "arraySelected")
        
        if (array?.count)! > 0 {
            let alertController = UIAlertController(title: "Seleziona Data", message:"\n\n\n\n\n\n\n\n " , preferredStyle: UIAlertControllerStyle.actionSheet)
            
            
            self.datePicker.frame = CGRect(x: alertController.view.frame.width/2 - 135, y: 20, width: 250, height: 200)
            alertController.view.addSubview(self.datePicker)
            alertController.view.frame = CGRect(x: 0, y: 0, width: 300, height: 200)
            alertController.view.bounds = CGRect(x: 0, y: 0, width: 300, height: 200)
            
            
            let cancelAction = UIAlertAction(title: "Cancella", style: .destructive) { (action) in
                
                
                
            }
            
            let pickAction = UIAlertAction(title: "Conferma", style: UIAlertActionStyle.cancel, handler: {
                
                (action : UIAlertAction!) -> Void in
                
                let saving = PFObject(className: "InBooking")
                saving["userId"] = PFUser.current()?.objectId
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy*HH:mm"
                
                let stringdate = dateFormatter.string(from: self.datePicker.date)
                saving["date"] = stringdate
                saving["state"] = "p"
                
                var stringServices = ""
                
                if (array?.count)! > 1 {
                    
                    var a = 0
                    for elem in array! {
                        if a == 0 {
                            
                            stringServices = stringServices + (elem as! String)
                            
                        } else {
                            
                            stringServices = stringServices + "*" + (elem as! String)
                        }
                        a = a + 1
                    }

                    
                } else {
                    
                    stringServices = array?.first as! String
                    
                }
                
                let user = UserDefaults()
                let idHotel = user.string(forKey: "idHotel")
                let idService = user.string(forKey: "idService")
                
                saving["subServices"] = "1@" + stringServices
                saving["hotelId"] = idHotel
                saving["serviceId"] = idService
                
                saving.saveInBackground()
                
                let banner = Banner(title: "Notifica", subtitle: "Sto inviando la richiesta", image: nil, backgroundColor: UIColor.init(red: 221/255, green: 199/255, blue: 134/255, alpha: 0.9))
                banner.dismissesOnTap = true
                banner.show(duration: 1)
                
                
            })
            
        
            alertController.addAction(cancelAction)
            alertController.addAction(pickAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func dismiss () {
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 7
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 + 1 {
            
            return 200
            
        } else if indexPath.row == 1 + 1 {
            
            return 80
            
        } else if indexPath.row == 2 + 1 {
            
            return 70
            
        } else if indexPath.row == 3 + 1 {
            
            return 0
            
        } else if indexPath.row == 4 + 1 {
            
            return 200
            
        } else if indexPath.row == 5 + 1 {
            
            return CGFloat(heightCell)
            
        } else if indexPath.row == 0 {
            
            return 64
            
        } else {
            return 0
        }
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
