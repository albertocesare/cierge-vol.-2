//
//  pendingTableViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 04/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import ParseUI
import RealmSwift

class pendingTableViewController: PFQueryTableViewController {
    
    var arrayPF : [PFObject] = []
    
    override func queryForTable() -> PFQuery<PFObject> {
        tableView.tableFooterView = UIView()
        let query = PFQuery(className: "UserCheckin")
        let userId = PFUser.current()?.objectId
        query.whereKey("active", equalTo: true)
        query.whereKey("userId", equalTo: userId!)
        query.whereKey("state", equalTo: "p")
        
        
        let queryy = PFQuery(className: "UserCheckin")
        queryy.whereKey("userId", equalTo: userId!)
        query.whereKey("active", equalTo: true)
        queryy.whereKey("state", equalTo: "p")
        queryy.countObjectsInBackground()
            {(count, error) in
                
                if (count < 1) {
                    
                    let textView : UITextView = UITextView()
                    textView.frame = CGRect(x: 20, y: self.view.frame.height / 2 - 50, width: self.view.frame.width - 40, height: 100)
                    textView.textAlignment = .center
                    textView.text = "In questa pagina visualizzerai le prenotazioni raggruppate in base ai soggiorni in corso e passati"
                    textView.font = UIFont.systemFont(ofSize: 15)
                    textView.textColor = .darkGray
                    self.tableView.tableFooterView?.addSubview(textView)
                }
                
        }
        
        return query
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, object: PFObject?) -> PFTableViewCell? {
        arrayPF.append(object!)
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TTableViewCell
        cell.accessoryType = .none
        let realm = try! Realm()
        let hotels = realm.objects(Hotel.self)
        for hotel in hotels {
            if (hotel.id == object?["hotelId"] as! String) {
                
                let name = hotel.name
                cell.labelTitle.text = name
                let arrName = name.components(separatedBy: " ")
                if arrName.count == 1 {
                    cell.buttonColor.setTitle(name.characters.first?.description, for: .normal)
                } else {
                    cell.buttonColor.setTitle((arrName[0].characters.first?.description)! + (arrName[1].characters.first?.description)!, for: .normal)
                }
                
            }
        }
        
        cell.labelSubtitle.text = "Aspettando le date di check"
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    
}
