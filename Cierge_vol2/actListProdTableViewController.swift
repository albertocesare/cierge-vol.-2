//
//  actListProdTableViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 10/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Parse

class actListProdTableViewController: UITableViewController {
    
    var arrayProd : [String] = []
    
    var arraySelected : [String] = []
    
    let datePicker : UIDatePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.allowsMultipleSelection = false
        
        let idService = UserDefaults.standard.string(forKey: "idService")
        
        let query = PFQuery(className: "Activities")
        query.whereKey("objectId", equalTo: idService!)
        query.findObjectsInBackground(block: { (objects, error) in
            
            let obj = objects?.first
            let info = (obj?["subServices"] as! String).components(separatedBy: "@").last?.components(separatedBy: "*")
            for inf in info! {
                
                if inf.components(separatedBy: "_").first == "a" {
                    self.arrayProd.append(inf)
                    print(inf)
                    self.tableView.reloadData()
                }
                
            }
            
        })
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arrayProd.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let info = arrayProd[indexPath.row]
        let name = info.components(separatedBy: "_")[1]
        let price = info.components(separatedBy: "_").last
        
        cell.textLabel?.text = name
        
        let priceLabel = UILabel(frame: CGRect(x: cell.frame.width - 100, y: cell.frame.height / 2 - 10, width: 60, height: 20))
        priceLabel.text = price! + " €"
        priceLabel.textAlignment = .right
        cell.addSubview(priceLabel)
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let userDef = UserDefaults.standard
        userDef.setValue(self.arrayProd[indexPath.row], forKey: "arraySelected")
        tableView.deselectRow(at: indexPath, animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "actList") , object: nil)
        
    }
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
