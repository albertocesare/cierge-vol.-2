//
//  pendingViewController.swift
//  Cierge_vol2
//
//  Created by Alberto Cesare Barbon on 04/05/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import ParseUI
import RealmSwift

class pendingViewController: PFQueryTableViewController {

    var arrayPF : [PFObject] = []
    
    override func queryForTable() -> PFQuery<PFObject> {
        
        tableView.tableFooterView = UIView()
        
        let query = PFQuery(className: "Purchasing")
        query.whereKey("state", equalTo: "p")
        query.cachePolicy = .networkElseCache
        query.whereKey("userId", equalTo: (PFUser.current()?.objectId)!)
        
        let queryy = PFQuery(className: "Purchasing")
        queryy.whereKey("userId", equalTo: (PFUser.current()?.objectId)!)
        queryy.whereKey("state", equalTo: "p")
        queryy.countObjectsInBackground()
            {(count, error) in
                
                if (count < 1) {
                    
                    let textView : UITextView = UITextView()
                    textView.frame = CGRect(x: 20, y: self.view.frame.height / 2 - 50, width: self.view.frame.width - 40, height: 100)
                    textView.textAlignment = .center
                    textView.text = "In questa pagina visualizzerai i tuoi acquisti in attesa di essere approvati dall'Hotel"
                    textView.font = UIFont.systemFont(ofSize: 15)
                    textView.textColor = .darkGray
                    self.tableView.tableFooterView?.addSubview(textView)
                }
        }
        
        
        return query
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, object: PFObject?) -> PFTableViewCell? {
        arrayPF.append(object!)
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TTableViewCell
        
        cell.accessoryType = .none
        
        let realm = try! Realm()
        let products = realm.objects(Product.self)
        for product in products {
            print(product.id)
            print(object?["productId"] as! String)
            if (product.id == object?["productId"] as! String) {
                
                let name = product.name
                cell.labelTitle.text = name
                cell.labelSubtitle.text = product.info.components(separatedBy: "*")[0]
                
                let arrName = name.components(separatedBy: " ")
                if arrName.count == 1 {
                    cell.buttonColor.setTitle(name.characters.first?.description, for: .normal)
                } else {
                    cell.buttonColor.setTitle((arrName[0].characters.first?.description)! + (arrName[1].characters.first?.description)!, for: .normal)
                }
                
            }
        }
        
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

}
